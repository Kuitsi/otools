<?php
/**
 * Sekalaisia apufunktioita erilaisiin laskureihin selainpeliä OGame varten
 * sekä niiden toimintaa esitteleviä laskureita.
 *
 * @author Kuitsi (http://iki.fi/kuitsi)
 * @license GPLv3 (see OTools/LICENSE.txt)
 */

// rajoitetaan PHP:n virheraportointia: E_NOTICE herjaa useammastakin kohdasta mutta ei vaikuta toimintaan, joten ei näytetä sitä ;)
error_reporting(E_ERROR | E_WARNING | E_PARSE);

header("content-type: text/html; charset=utf-8");

/**
 * Laskee yhteen kaksi kellonaikaa ja ottaa huomioon vuorokauden vaihtumisen
 * Haluaa ajat muodossa hh:mm:ss. Palauttaa ajan samassa muodossa
 */
function sumTime($eka,$toka) {

	if ( !preg_match("/^\d{1,2}:\d{1,2}:\d{1,2}$/", $eka) || !preg_match("/^\d{1,2}:\d{1,2}:\d{1,2}$/", $toka) )
		return "Virheellinen aika!";

	list($h1,$m1,$s1)=split(":",$eka);
	list($h2,$m2,$s2)=split(":",$toka);

	if (
		($h1 < 0 || $h1 > 23) ||
		($h2 < 0 || $h2 > 23) ||
		($m1 < 0 || $m1 > 59) ||
		($m2 < 0 || $m2 > 59) ||
		($s1 < 0 || $s1 > 59) ||
		($s2 < 0 || $s2 > 59)
		) return "Virheellinen aika!";

	// muunnetaan ajat sekunneiksi
	$sekunnit1 = $h1*3600 + $m1*60 + $s1;
	$sekunnit2 = $h2*3600 + $m2*60 + $s2;

	$sekunnit3 = $sekunnit1 + $sekunnit2;

	$h3 = floor($sekunnit3 / 3600);
	$m3 = floor($sekunnit3 % 3600 / 60);
	$s3 = $sekunnit3 % 60;

	// huomioidaan päivän vaihtuminen
	if ($h3 > 23) $h3 = $h3-24;

	// etunollat numeroihin
	if ($h3 < 10) $h3 = "0$h3";
	if ($m3 < 10) $m3 = "0$m3";
	if ($s3 < 10) $s3 = "0$s3";

	//tuloksen muotoilu
	$tulos="$h3:$m3:$s3";

	return $tulos;
}

/**
 * Vähentää ensimmäisestä kellonajasta jälkimmäisen ja ottaa huomioon vuorokauden vaihtumisen
 * Haluaa ajat muodossa hh:mm:ss. Palauttaa ajan samassa muodossa
 */
function diffTime($eka, $toka) {

	if ( !preg_match("/^\d{1,2}:\d{1,2}:\d{1,2}$/", $eka) || !preg_match("/^\d{1,2}:\d{1,2}:\d{1,2}$/", $toka) )
		return "Virheellinen aika!";

	list($h1,$m1,$s1)=split(":",$eka);
	list($h2,$m2,$s2)=split(":",$toka);

	if (
		($h1 < 0 || $h1 > 23) ||
		($h2 < 0 || $h2 > 23) ||
		($m1 < 0 || $m1 > 59) ||
		($m2 < 0 || $m2 > 59) ||
		($s1 < 0 || $s1 > 59) ||
		($s2 < 0 || $s2 > 59)
		) return "Virheellinen aika!";

	// muunnetaan ajat sekunneiksi
	$sekunnit1 = $h1*3600 + $m1*60 + $s1;
	$sekunnit2 = $h2*3600 + $m2*60 + $s2;

	if ($sekunnit1==$sekunnit2) {
		$tulos="00:00:00";
		return $tulos;
	}

	$sekunnit3 = $sekunnit1 - $sekunnit2;

	// huomioidaan päivän vaihtuminen
	if ($sekunnit3 < 0) $sekunnit3 = $sekunnit3 + 24*60*60;

	$h3 = floor($sekunnit3 / 3600);
	$m3 = floor($sekunnit3 % 3600 / 60);
	$s3 = $sekunnit3 % 60;

	// etunollat numeroihin
	if ($h3 < 10) $h3 = "0$h3";
	if ($m3 < 10) $m3 = "0$m3";
	if ($s3 < 10) $s3 = "0$s3";

	//tuloksen muotoilu
	$tulos="$h3:$m3:$s3";

	return $tulos;
}

/**
 * Muuntaa $sekunnit (INT) muotoon hh:mm:ss
 * Jos $päivät on true, palauttaa taulukon, jossa ensimmäisessä
 * solussa päivät (INT), toisessa solussa aika muodossa hh:mm:ss
 */
function secondsToTime($sekunnit, $päivät = false) {

  if ( !preg_match("/^\d+$/", $sekunnit) )
	return "Virheellinen aika!";

  $d = floor($sekunnit / 86400);             // päivät   86400s = 3600*24
  $h = floor(($sekunnit % 86400) / 3600);    // tunnit   3600s = 60*60
  $m = floor(($sekunnit % 3600) / 60);       // minuutit 60s
  $s = $sekunnit % 60;                       // sekunnit

  // etunollat numeroihin
  if ($h < 10) $h = "0$h";
  if ($m < 10) $m = "0$m";
  if ($s < 10) $s = "0$s";

  if ($päivät) {
	$tulos[0] = $d;
	$tulos[1] = "$h:$m:$s";
  }
  else $tulos = "$h:$m:$s";

  return $tulos;
}

/**
 * Palauttaa lentoajan sekunneissa (INT) kun tiedetään käytettävän aluksen todellinen nopeus ($nopeus),
 * lentonopeus prosenteissa ($prosentit), universumin nopeus ($univNopeus) sekä lennon molemmat koordinaatit ($lähtökoordit ja $kohdekoordit).
 */
function lentoaika($nopeus, $prosentit, $lähtökoordit, $kohdekoordit, $univNopeus=1) {

  if ( !preg_match("/^\d+$/", $nopeus) )
	return "Virheellinen nopeus!";
  if ( !preg_match("/^\d{1}:\d{1,3}:\d{1,2}$/", $lähtökoordit) || !preg_match("/^\d{1}:\d{1,3}:\d{1,2}$/", $kohdekoordit) )
	return "<b>Virheelliset koordinaatit!</b>";
	
  list($galaksi1,$systeemi1,$planeetta1)=split(":",$lähtökoordit);
  list($galaksi2,$systeemi2,$planeetta2)=split(":",$kohdekoordit);

  // lasketaan etäisyys
  if ($galaksi1 != $galaksi2)
    $etäisyys = 20000 * abs($galaksi1 - $galaksi2);
  else if ($systeemi1 != $systeemi2)
	$etäisyys = 95 * abs($systeemi1 - $systeemi2) + 2700;
  else if ($planeetta1 != $planeetta2)
	$etäisyys = 5 * abs($planeetta1 - $planeetta2) + 1000;
  else $etäisyys = 5;
  
  // ja varsinainen lentoaika perustuen lentomatkaan
  $sekunnit = 10 + (35000 / $prosentit * sqrt( $etäisyys * 1000 / $nopeus));

  return round($sekunnit / $univNopeus);
}

/**
 * Funktio palauttaa aluksen todellisen nopeuden (INT)
 * $poltto = Polttoajon taso (INT)
 * $impulssi = Impulssiajon taso (INT)
 * $hyper = Hyperajon taso (INT)
 * $tyyppi = Aluksen tyyppi (String). Katso lyhenteet $perusnopeus-taulukosta alapuolelta
 */
function nopeus($poltto,$impulssi,$hyper,$tyyppi){

  $perusnopeus = array(
	'PR' => 5000,      // Pieni rahtialus 
	'SR' => 7500,      // Suuri rahtialus
	'KH' => 12500,     // Kevyt Hävittäjä
	'RH' => 10000,     // Raskas Hävittäjä
	'RS' => 15000,     // Risteilijä
	'TA' => 10000,     // Taistelualus
	'TR' => 10000,     // Taisteluristeilijä
	'TH' => 5000,      // Tuhoaja
	'KT' => 100,       // Kuolemantähti
	'PM' => 4000,      // Pommittaja
	'KR' => 2000,      // Kierrättäjä
	'VL' => 100000000, // Vakoiluluotain
	'SA' => 2500);     // Siirtokunta-alus

  // laskee ja palauttaa alusten todellisen nopeuden
  if ($tyyppi=='PR' && $impulssi >= 5)
	return 10000 * (1 + ($impulssi * 0.2));
  if ($tyyppi=='PM' && $hyper >= 8)
	return 5000 * (1 + ($hyper * 0.3));
  if ($tyyppi=='PR' || $tyyppi=='SR' || $tyyppi=='KH' || $tyyppi=='KR' || $tyyppi=='VL')
	return $perusnopeus[$tyyppi] * (1 + ($poltto * 0.1));
  if ($tyyppi=='RH' || $tyyppi=='RS' || $tyyppi=='PM' || $tyyppi=='SA')
	return $perusnopeus[$tyyppi] * (1 + ($impulssi * 0.2));
  if ($tyyppi=='TA' || $tyyppi=='TR' || $tyyppi=='TH' || $tyyppi=='KT')
	return $perusnopeus[$tyyppi] * (1 + ($hyper * 0.3));
}

function isShipChecked($alus) {
	if (isset($_POST["alukset"]) && in_array($alus, $_POST["alukset"]))
		echo ' checked="checked"';
}

echo '<?xml version="1.0" encoding="UTF-8"?>'."\n"; ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="fi" lang="fi">
<head>
  <title>Aikalaskureita selainpeliin OGame</title>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
  <style type="text/css">
    abbr[title] {cursor: help;}
  </style>
</head>

<body>
<div style="width: 700px;">

<h1>Aikalaskureita selainpeliin OGame</h1>

<p>Näillä laskureilla voit laskea muutamia tärkeitä laskuja taisteluihin liittyen, ilman Neogamen tai SuperOgamen kaltaisia selaimen lisäosia.
(Tarvittaessa voit käyttää <i>puolustusajan</i> laskinta myös tavalliseen aikojen yhteenlaskuun ja vastaavasti <i>Puolustus/Ninjauslaivueen
lähettämisajan</i> laskinta aikojen vähennyslaskuun.)</p>

<p>Ninjassa lähtöaika on muutaman sekunnin lasketun lähtöajan jälkeen ja puolustamaan mentäessä n. 10 sekuntia sitä aikaisemmin.</p>

<a name="lentoajat"></a><h2>Lentoajat</h2>

<p>Laskee lentoajan kahden pisteen välillä ja kertoo myös saapumisajan.</p>

<table>
	<form method="post" action="<?php echo $_SERVER["PHP_SELF"]."#lentoajat"; ?>">
		<input type="hidden" name="mode" value="lentoajat" />
	<tr>
		<td>Universumin nopeus:</td>
		<td>
		  <label for="univ1x">1x</label>
		  <input type="radio" name="univNopeus" id="univ1x" value="1"<?php if ($_POST["univNopeus"]=="1" || !isset($_POST["univNopeus"])) echo ' checked="checked"'; ?> />
		  <label for="univ2x">2x</label>
		  <input type="radio" name="univNopeus" id="univ2x" value="2"<?php if ($_POST["univNopeus"]=="2") echo ' checked="checked"'; ?> />
		</td>
	</tr>
	<tr>
		<td valign="top">Alukset:</td>
		<td>
		  <table cellpadding="5">
			<tr>
			  <td><input type="checkbox" name="alukset[]" value="PR"<?php isShipChecked("PR"); ?> /> <abbr title="Pieni rahtialus">PR</abbr></td>
			  <td><input type="checkbox" name="alukset[]" value="SR"<?php isShipChecked("SR"); ?> /> <abbr title="Suuri rahtialus">SR</abbr></td>
			  <td><input type="checkbox" name="alukset[]" value="KH"<?php isShipChecked("KH"); ?> /> <abbr title="Kevyt hävittäjä">KH</abbr></td>
			  <td><input type="checkbox" name="alukset[]" value="RH"<?php isShipChecked("RH"); ?> /> <abbr title="Raskas hävittäjä">RH</abbr></td>
			  <td><input type="checkbox" name="alukset[]" value="RS"<?php isShipChecked("RS"); ?> /> <abbr title="Risteilijä">RS</abbr></td>
			  <td><input type="checkbox" name="alukset[]" value="TA"<?php isShipChecked("TA"); ?> /> <abbr title="Taistelualus">TA</abbr></td>
			  <td><input type="checkbox" name="alukset[]" value="SA"<?php isShipChecked("SA"); ?> /> <abbr title="Siirtokunta-alus">SA</abbr></td>
			</tr>
			<tr>
			  <td><input type="checkbox" name="alukset[]" value="KR"<?php isShipChecked("KR"); ?> /> <abbr title="Kierrättäjä">KR</abbr></td>
			  <td><input type="checkbox" name="alukset[]" value="VL"<?php isShipChecked("VL"); ?> /> <abbr title="Vakoiluluotain">VL</abbr></td>
			  <td><input type="checkbox" name="alukset[]" value="PM"<?php isShipChecked("PM"); ?> /> <abbr title="Pommittaj">PM</abbr></td>
			  <td><input type="checkbox" name="alukset[]" value="TH"<?php isShipChecked("TH"); ?> /> <abbr title="Tuhoaja">TH</abbr></td>
			  <td><input type="checkbox" name="alukset[]" value="KT"<?php isShipChecked("KT"); ?> /> <abbr title="Kuolemantähti">KT</abbr></td>
			  <td><input type="checkbox" name="alukset[]" value="TR"<?php isShipChecked("TR"); ?> /> <abbr title="Taisteluristeilijä">TR</abbr></td>
			  <td></td>
			</tr>
		  </table>
		</td>
	</tr>
	<tr>
		<td>Kääntymisaika:</td>
		<td><input name="kääntymisaika" type="text" maxlength="8" size="8" value="<?php if (isset($_POST["kääntymisaika"])) echo $_POST["kääntymisaika"]; else echo "hh:mm:ss"; ?>"
			onClick="if(this.value == 'hh:mm:ss') this.value = '';" onBlur="if(this.value == '') this.value= 'hh:mm:ss';" /></td>
	</tr>
	<tr>
		<td>Kääntymiskoordinaatit:</td>
		<td><input name="kääntymiskoordinaatit" type="text" maxlength="8" size="8" value="<?php if (isset($_POST["kääntymiskoordinaatit"])) echo $_POST["kääntymiskoordinaatit"]; else echo "g:sss:pp"; ?>"
			onClick="if(this.value == 'g:sss:pp') this.value = '';" onBlur="if(this.value == '') this.value= 'g:sss:pp';" /></td>
	</tr>
	<tr>
		<td>Paluukoordinaatit:</td>
		<td><input name="paluukoordinaatit" type="text" maxlength="8" size="8" value="<?php if (isset($_POST["paluukoordinaatit"])) echo $_POST["paluukoordinaatit"]; else echo "g:sss:pp"; ?>"
			onClick="if(this.value == 'g:sss:pp') this.value = '';" onBlur="if(this.value == '') this.value= 'g:sss:pp';" /></td>
	</tr><?php

	// käytetään ensisijaisesti syötettyjä tekkeja, varalla 0
	$poltto = ( preg_match("/^\d+$/", $_POST["poltto"]) ? $_POST["poltto"] : 0 );
	$impulssi = ( preg_match("/^\d+$/", $_POST["impulssi"]) ? $_POST["impulssi"] : 0 );
	$hyper = ( preg_match("/^\d+$/", $_POST["hyper"]) ? $_POST["hyper"] : 0 );

	?>
	<tr>
		<td>Ajoteknologiat:</td>
		<td>
			Poltto: <input name="poltto" type="text" maxlength="2" size="2" value="<?php if (isset($poltto)) echo $poltto; ?>" />
			Impulssi: <input name="impulssi" type="text" maxlength="2" size="2" value="<?php if (isset($impulssi)) echo $impulssi; ?>" />
			Hyper: <input name="hyper" type="text" maxlength="2" size="2" value="<?php if (isset($hyper)) echo $hyper; ?>" />
		</td>
	</tr>
	<tr>
		<td colspan="2"><input type="submit" value="Laske" /></td>
	</tr>
	</form>
</table>
<?php
if ($_POST["mode"]=="lentoajat") {

  if ( !preg_match("/^\d{1}:\d{1,3}:\d{1,2}$/", $_POST["kääntymiskoordinaatit"]) || !preg_match("/^\d{1}:\d{1,3}:\d{1,2}$/", $_POST["paluukoordinaatit"]) )
	echo "<p>Virheelliset koordinaatit!</p>\n";
	
  // näytetään tulokset vain jos aluksia valittu
  if (isset($_POST["alukset"])) {
	
	// etsitään hitain mukana oleva alus
	foreach ($_POST["alukset"] as $alus) {
	  $nopeudet[$alus] = nopeus($poltto, $impulssi, $hyper, $alus);
	}
	asort($nopeudet); // järjestää taulukon säilyttäen avain->arvo -parit ehjinä
	
	echo "<p>Hitain alus ".key($nopeudet).", nopeus ".current($nopeudet).". Käytetyt moottoriteknologiat $poltto, $impulssi, $hyper.</p>\n";

	// tulostetaan lentoaikataulukko eri nopeuksilla
	$prosentit = array(100, 90, 80, 70, 60, 50, 40, 30, 20, 10); // lentonopeudet
	echo "<table cellpadding=\"5\">\n";
	echo "\t<tr><td>Nopeus</td><td>Lentoaika</td><td>Saapumisaika</td></tr>\n";
	foreach ($prosentit as $p) {
	  $lentoaika = secondsToTime(lentoaika(current($nopeudet), $p, $_POST["kääntymiskoordinaatit"], $_POST["paluukoordinaatit"], $_POST["univNopeus"]));
	  echo "\t<tr><td>$p%</td><td>".$lentoaika."</td><td>".sumTime($lentoaika, $_POST["kääntymisaika"])."</td></tr>\n";
	}
	echo "</table>\n";
  } else echo "<p>Valitse alukset!</p>\n";

} // lentoaikojen laskeminen

?>

<a name="puolustusaika"></a><h2>Puolustusaika</h2>

<table>
	<form method="post" action="<?php echo $_SERVER["PHP_SELF"]."#puolustusaika"; ?>">
		<input type="hidden" name="mode" value="puolustus" />
	<tr>
		<td>Palvelinaika:</td>
		<td><input name="puolustaja_palvelinaika" type="text" maxlength="8" size="8" value="<?php if (isset($_POST["puolustaja_palvelinaika"])) echo $_POST["puolustaja_palvelinaika"]; else echo "hh:mm:ss"; ?>"
			onClick="if(this.value == 'hh:mm:ss') this.value = '';" onBlur="if(this.value == '') this.value= 'hh:mm:ss';" /></td>
	</tr>
	<tr>
		<td>Vihun tuloon aikaa:</td>
		<td><input name="puolustaja_vihu" type="text" maxlength="8" size="8" value="<?php if (isset($_POST["puolustaja_vihu"])) echo $_POST["puolustaja_vihu"]; else echo "hh:mm:ss"; ?>"
			onClick="if(this.value == 'hh:mm:ss') this.value = '';" onBlur="if(this.value == '') this.value= 'hh:mm:ss';" /></td>
	</tr><?php

if ($_POST["mode"]=="puolustus")
	echo "\n\t<tr>\n\t\t<td>Isku pallolla:</td>\n\t\t<td><b>".sumTime($_POST["puolustaja_palvelinaika"],$_POST["puolustaja_vihu"])."</b></td>\n\t</tr>\n";

?>
	<tr>
		<td colspan="2"><input type="submit" value="Laske" /></td>
	</tr>
	</form>
</table>

<a name="ninjausaika"></a><h2>Ninjausaika</h2>

<p>Tarvittavan <i>vihun lentoaika takaisin</i> saat laskettua kätevästi <a href="http://www.o-calc.com/?sec=_timer&amp;lang=fi" target="_blank">http://www.o-calc.com/?sec=_timer&amp;lang=fi</a>
löytyvällä laskurilla (aukeaa uuteen ikkunaan).</p>

<table>
	<form method="post" action="<?php echo $_SERVER["PHP_SELF"]."#ninjausaika"; ?>">
		<input type="hidden" name="mode" value="ninja" />
	<tr>
		<td>Palvelinaika:</td>
		<td><input name="ninja_palvelinaika" type="text" maxlength="8" size="8" value="<?php if (isset($_POST["ninja_palvelinaika"])) echo $_POST["ninja_palvelinaika"]; else echo "hh:mm:ss"; ?>"
			onClick="if(this.value == 'hh:mm:ss') this.value = '';" onBlur="if(this.value == '') this.value= 'hh:mm:ss';" /></td>
	</tr>
	<tr>
		<td>Vihun tuloon aikaa:</td>
		<td><input name="ninja_tulo" type="text" maxlength="8" size="8" value="<?php if (isset($_POST["ninja_tulo"])) echo $_POST["ninja_tulo"]; else echo "hh:mm:ss"; ?>"
			onClick="if(this.value == 'hh:mm:ss') this.value = '';" onBlur="if(this.value == '') this.value= 'hh:mm:ss';" /></td>
	</tr>
	<tr>
		<td>Vihun lentoaika takaisin:</td>
		<td><input name="ninja_paluu" type="text" maxlength="8" size="8" value="<?php if (isset($_POST["ninja_paluu"])) echo $_POST["ninja_paluu"]; else echo "hh:mm:ss"; ?>"
			onClick="if(this.value == 'hh:mm:ss') this.value = '';" onBlur="if(this.value == '') this.value= 'hh:mm:ss';" /></td>
	</tr><?php

if ($_POST["mode"]=="ninja")
	echo "\n\t<tr>\n\t\t<td>Vihu kotona:</td>\n\t\t<td><b>".sumTime($_POST["ninja_tulo"], sumTime($_POST["ninja_palvelinaika"],$_POST["ninja_paluu"]))."</b></td>\n\t</tr>\n";

?>
	<tr>
		<td colspan="2"><input type="submit" value="Laske" /></td>
	</tr>
	</form>
</table>

<a name="lahetysaika"></a><h2>Puolustus/Ninjauslaivueen lähettämisaika</h2>

<table>
	<form method="post" action="<?php echo $_SERVER["PHP_SELF"]."#lahetysaika"; ?>">
		<input type="hidden" name="mode" value="lähtöaika" />
	<tr>
		<td>Isku / Vihu kotona:</td>
		<td><input name="lähtö_isku" type="text" maxlength="8" size="8" value="<?php if (isset($_POST["lähtö_isku"])) echo $_POST["lähtö_isku"]; else echo "hh:mm:ss"; ?>"
			onClick="if(this.value == 'hh:mm:ss') this.value = '';" onBlur="if(this.value == '') this.value= 'hh:mm:ss';" /></td>
	</tr>
	<tr>
		<td>Oma lentoaika:</td>
		<td><input name="lähtö_lentoaika" type="text" maxlength="8" size="8" value="<?php if (isset($_POST["lähtö_lentoaika"])) echo $_POST["lähtö_lentoaika"]; else echo "hh:mm:ss"; ?>"
			onClick="if(this.value == 'hh:mm:ss') this.value = '';" onBlur="if(this.value == '') this.value= 'hh:mm:ss';" /></td>
	</tr><?php

if ($_POST["mode"]=="lähtöaika")
	echo "\n\t<tr>\n\t\t<td>Lähtöaika:</td>\n\t\t<td><b>".diffTime($_POST["lähtö_isku"],$_POST["lähtö_lentoaika"])."</b></td>\n\t</tr>\n";

?>
	<tr>
		<td colspan="2"><input type="submit" value="Laske" /></td>
	</tr>
	</form>
</table>

</div>
</body>
</html>