<?php
/**
 * LTS-tuotonjakolaskuri
 * @author Kuitsi (http://iki.fi/kuitsi)
 * @license GPLv3 (see OTools/LICENSE.txt)
 *
 * Laskee LTS-hyökkäyksestä saadut voitot tasan ottaen huomioon ensin kärsityt tappiot
 * TODO: tappiollisen deuteriumin muunto muiksi resuiksi
 */

// rajoitetaan PHP:n virheraportointia: E_NOTICE herjaa useammastakin kohdasta mutta ei vaikuta toimintaan, joten ei näytetä sitä ;)
error_reporting(E_ERROR | E_WARNING | E_PARSE);

// ei sallita välimuistin käyttöä
header("Expires: Mon, 26 Jul 1997 05:00:00 GMT");// Date in the past
header("Cache-Control: no-cache");
header("Pragma: no-cache");

require("taistelu.php");
require("raportinlukija.php");

echo '<?xml version="1.0" encoding="UTF-8"?>'."\n";

/**
 * Lista alusten lyhenteistä ja koko nimistä.
 * Koko nimet näytetään tooltippeinä lomakkeessa ja
 * lyhenteitä käytetään skriptin muissa osioissa.
 *
 * Alusten hinnat on määritelty pelaaja.php:ssa.
 * 2-kirjaimiset lyhenteet on täsmättävä
 * raportinlukija.php:n SEKÄ pelaaja.php:n kanssa
 */
 $alustenNimet = array(
  'pr' => 'Pieni rahtialus',
  'sr' => 'Suuri rahtialus',
  'kh' => 'Kevyt hävittäjä',
  'rh' => 'Raskas hävittäjä',
  'rs' => 'Risteilijä',
  'ta' => 'Taistelualus',
  'sa' => 'Siirtokunta-alus',
  'kr' => 'Kierrättäjä',
  'vl' => 'Vakoiluluotain',
  'pm' => 'Pommittaja',
  'th' => 'Tuhoaja',
  'kt' => 'Kuolemantähti',
  'tr' => 'Taisteluristeilijä'
 );

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="fi" xml:lang="fi">
<head>
	<title>LTS-hyökkäyksen tuotonjakolaskin</title>
	<meta http-equiv="Content-Type" content="application/text+html; charset=UTF-8" />	
	<style type="text/css">
	  abbr[title] {cursor: help;}
	  #wrapper {width: 900px;}
	  #tulosOtsikko td {font-weight:bold;}
	  .error { font-weight: bold; color: #ff0000; font-size: 20px; }
	</style>
</head>

<body style="background-image: url(background.jpg); background-repeat: no-repeat; background-position: top right; background-attachment:fixed;" text="#ffffff" link="#ffff00" vlink="#ffff00" bgcolor="#050E1F">
  <div id="wrapper">

  <h1>LTS-hyökkäyksen tuotonjakolaskin</h1>
  
  <?php
if (isset($_GET["lue"])) { ?>

  <h2>Lue taisteluraportti</h2>
  
  <form action="<?php echo $_SERVER["PHP_SELF"]; ?>" method="post">
	<input type="hidden" name="rapsaLuettu" value="true" />
  
  Kopioi koko taisteluraportti tähän:<br />
  <textarea name="raportti" rows="20" cols="100"></textarea>
  
  <p><input type="submit" value="Lue raportti" /></p>
  
  </form>

  <hr />
<?php }

else {

  if ($_POST["rapsaLuettu"]) {
	$raportinlukija = new Raportinlukija($_POST["raportti"]);
	//echo $raportinlukija->getPelaajia()."<pre>"; print_r($raportinlukija); echo "</pre>";
  }
?>

  <p>Jakaa LTS-taistelun tuotot tasan. <a href="<?php echo $_SERVER["PHP_SELF"]; ?>?lue">Raportinlukija</a> täyttää hajonneiden alusten lukumäärät valmiiksi.
  Ottaa huomioon jokaisen kärsimät tappiot (hajonneet alukset, polttoaine, ammutut ohjukset) sekä saadut tulot (kaapatut ja kierrätetyt resut). <del>Muuntaa
  myös tappiolle jäädyt deuteriumit muiksi resuiksi suhteella 2:1:1.</del></p>
  
  Jokaisen pelaajan on kerättävä taistelusta seuraavat tiedot:<br />
  <ul style="margin-top: 0px;">
	<li>Kaikkien omien laivueiden yhteenlaskettu deuteriumin kulutus, myös kierrättäjät</li>
	<li>Kaikkien omien laivueiden yhteensä kaappaamat resurssit</li>
	<li>Itse kierrättämät romut</li>
	<li>Ammuttujen ohjusten lukumäärä</li>
  </ul>
   
  <p><b>HUOM!</b> Numeroissa ei saa olla mitään tuhaterottimia (pisteitä, välilyöntejä) eikä desimaalipilkkuja!</p>
  
  <hr />
  
  <form action="<?php echo $_SERVER["PHP_SELF"] ?>#tulos" method="post">
	<input type="hidden" name="laske" value="true" />
  
  <p><a href="<?php echo $_SERVER["PHP_SELF"]; ?>?lue">Lue alusten tiedot taisteluraportista</a> | <input type="submit" value="Laske tuotot" /></p>
  
  <hr />
  
  <p>Hyökkääjiä:
  <select name="pelaajia">
<?php
  
  // pelaajien lukumäärä ensisijaisesti raportinlukijalta, varalla lomakkeen edellinen arvo
  $pelaajia = ( ( is_object($raportinlukija) && !$raportinlukija->isError() ) ? $raportinlukija->getPelaajia() : $_POST["pelaajia"] );

  for($i=1; $i<6; $i++){
	echo "\t<option value=\"$i\"";
	if ($pelaajia == $i) echo " selected=\"selected\"";
	echo ">$i</option>\n";
  }
?>
  </select>
  </p>
  
  <?php
  $KAMet = ( $_POST["kierrApuMet"] != "" ? $_POST["kierrApuMet"] : "" );
  $KAKrist = ( $_POST["kierrApuKrist"] != "" ? $_POST["kierrApuKrist"] : "" );
  ?><p>Kierrätysapu: <input type="text" name="kierrApuMet" size="8" value="<?php echo $KAMet; ?>" /> metallia ja <input type="text" name="kierrApuKrist" size="8" value="<?php echo $KAKrist; ?>" /> kristallia. <br />
  (kuinka paljon hyökkäykseen osallistumattomat ovat auttaneet kierrättämisessä)</p>
  
  <!--<p><del>Deuttimuunnos ensisijaisesti</del>: (ei vielä käytössä)<br />
  <input type="radio" name="deuttimuunnos" id="dm_metalliksi" value="metalliksi" <?php
	  if ($_POST["deuttimuunnos"]=="metalliksi" || !isset($_POST["deuttimuunnos"])) echo 'checked="checked"'; ?>/>
	<label for="dm_metalliksi">metalliksi</label>
  <br />
  <input type="radio" name="deuttimuunnos" id="dm_kristalliksi" value="kristalliksi" <?php
	  if ($_POST["deuttimuunnos"]=="kristalliksi") echo 'checked="checked"'; ?>/>
	<label for="dm_kristalliksi">kristalliksi</label>
  </p>-->
  
  <hr />
  
  <?php
  
	// näytetään lopputulos ennen muita tietoja
	if ($_POST["laske"]) {
	  echo "<a name=\"tulos\"></a><h2>Tulos</h2>\n";//<p>Tiedot laskettu ".date("d.m.Y H:i:s")."</p>\n";
	  
	  // alustetaan taistelu ja syötetään sille kaikki tarpeelliset tiedot
	  $taistelu = new Taistelu($_POST["pelaajia"]);
	  for ($pelaaja=0; $pelaaja<$_POST["pelaajia"]; $pelaaja++) {
		$taistelu->getPelaaja($pelaaja)->setNimi($_POST["p".$pelaaja."_nimi"]);
		$taistelu->getPelaaja($pelaaja)->ohjuksiaAmmuttu($_POST["p".$pelaaja."_ohjuksiaAmmuttu"]);
		$taistelu->getPelaaja($pelaaja)->polttoainettaKulunut($_POST["p".$pelaaja."_kulutus"]);
		$taistelu->getPelaaja($pelaaja)->kaapattuMukaan(
				  $_POST["p".$pelaaja."_kaapattuMetalli"],
				  $_POST["p".$pelaaja."_kaapattuKristalli"],
				  $_POST["p".$pelaaja."_kaapattuDeuterium"]);
		$taistelu->getPelaaja($pelaaja)->kierratetty(
				  $_POST["p".$pelaaja."_kierratettyMetalli"],
				  $_POST["p".$pelaaja."_kierratettyKristalli"]);
		foreach ($alustenNimet as $lyhenne => $alus){
		  $taistelu->getPelaaja($pelaaja)->aluksiaMenetetty(array($lyhenne => $_POST["p".$pelaaja."_" . $lyhenne]));
		}
	  }
	  // TODO
	  //if ($_POST["deuttimuunnos"]=="kristalliksi") $taistelu->deuttimuunnos(true);
	  //else /*$_POST["deuttimuunnos"]=="metalliksi"*/ $taistelu->deuttimuunnos(false);
	  $osuus = $taistelu->laskeOsuus();
	  $tulos = $taistelu->haeTulos();
	  
	  //echo "<pre>"; print_r($taistelu); echo "</pre>\n";
	  
	  echo "\n<p>Negatiiviset arvot kertovat summan paljonko resuja pelaaja x joutuu yhteensä maksamaan muille, ".
		   "positiiviset kuinka paljon hän saa. Lisäksi jokainen saa kierrätysavusta yhtä paljon.</p>\n";
	  
	  echo "\n<table cellpadding=\"10\" border=\"0\">\n";
	  echo "\t<tr id=\"tulosOtsikko\">\n";
	  echo "\t\t<td>Pelaaja</td>\n";
	  echo "\t\t<td>Metalli</td>\n";
	  echo "\t\t<td>Kristalli</td>\n";
	  echo "\t\t<td>Deuterium</td>\n";
	  echo "\t</tr>\n";
	  for ($pelaaja=0; $pelaaja<$_POST["pelaajia"]; $pelaaja++) {
		$p = $taistelu->getPelaaja($pelaaja);
		echo "\t<tr>\n";
		echo "\t\t<td>".htmlspecialchars($p->getNimi())."</td>\n";
		echo "\t\t<td>".$tulos[$pelaaja]["met"]."</td>\n";
		echo "\t\t<td>".$tulos[$pelaaja]["krist"]."</td>\n";
		echo "\t\t<td>".$tulos[$pelaaja]["deut"]."</td>\n";
		echo "\t</tr>\n";
	  
	  }
	  if ($_POST["kierrApuMet"] != "" || $_POST["kierrApuKrist"] != "") {
		$kmet   = floor($_POST["kierrApuMet"] / $_POST["pelaajia"]);
		$kkrist = floor($_POST["kierrApuKrist"] / $_POST["pelaajia"]);
		echo "\t<tr>\n";
		echo "\t\t<td>Kierrätysapu</td>\n";
		echo "\t\t<td>". $kmet ."</td>\n";
		echo "\t\t<td>". $kkrist ."</td>\n";
		echo "\t</tr>";
	  }
		
	  echo "</table>\n";
	  
	  //echo "\n<p><b>debug:</b> Jokaiselle pelaajalle jäi kokonaissaldoksi ". ($osuus['met']+$kmet) ." metallia, ". ($osuus['krist']+$kkrist) ." kristallia ja ".  $osuus['deut'] ." deuteriumia.</p>\n";
	  
	  echo "\n<hr />\n";
	}
  
	// pelaajien tiedot taistelusta
	for($pelaaja=0; $pelaaja<5; $pelaaja++) { 
	
	// haetaan esitäytetyt tiedot lomakkeisiin
	$pelaajanNimi = ( ( is_object($raportinlukija) && !$raportinlukija->isError() ) ? $raportinlukija->getNimi($pelaaja) : "" );
	$nimi = ( $_POST["p".$pelaaja."_nimi"] != "" ? $_POST["p".$pelaaja."_nimi"] : $pelaajanNimi );
	$kulutus = ( $_POST["p".$pelaaja."_kulutus"] != "" ? $_POST["p".$pelaaja."_kulutus"] : "" );
	$ohjuksia = ( $_POST["p".$pelaaja."_ohjuksiaAmmuttu"] != "" ? $_POST["p".$pelaaja."_ohjuksiaAmmuttu"] : "" );
	$kaapMet = ( $_POST["p".$pelaaja."_kaapattuMetalli"] != "" ? $_POST["p".$pelaaja."_kaapattuMetalli"] : "" );
	$kaapKrist = ( $_POST["p".$pelaaja."_kaapattuKristalli"] != "" ? $_POST["p".$pelaaja."_kaapattuKristalli"] : "" );
	$kaapDeut = ( $_POST["p".$pelaaja."_kaapattuDeuterium"] != "" ? $_POST["p".$pelaaja."_kaapattuDeuterium"] : "" );
	$kierMet = ( $_POST["p".$pelaaja."_kierratettyMetalli"] != "" ? $_POST["p".$pelaaja."_kierratettyMetalli"] : "" );
	$kierKrist = ( $_POST["p".$pelaaja."_kierratettyKristalli"] != "" ? $_POST["p".$pelaaja."_kierratettyKristalli"] : "" );
	?>
  
  <h2>Pelaaja <?php echo $pelaaja+1 ?></h2>
  
  <table cellpadding="3">
	<tr>
	  <td>Pelaajan nimi</td>
	  <td><input type="text" name="p<?php echo $pelaaja; ?>_nimi" size="10" value="<?php echo $nimi; ?>" /></td>
	  <td></td>
	</tr>
	<tr>
	  <td>Polttoaineen kulutus</td>
	  <td><input type="text" name="p<?php echo $pelaaja; ?>_kulutus" size="10" value="<?php echo $kulutus; ?>" /></td>
	  <td>(kaikki laivueet yhteensä, myös kierrättäjät)</td>
	</tr>
	<tr>
	  <td>Ohjuksia ammuttu</td>
	  <td><input type="text" name="p<?php echo $pelaaja; ?>_ohjuksiaAmmuttu" size="3" value="<?php echo $ohjuksia; ?>" /></td>
	  <td></td>
	</tr>
  </table>
  
  <h3>Saadut resurssit</h3>
  
  <table cellpadding="3">
	<tr>
	  <td></td>
	  <td>Metallia</td>
	  <td>Kristallia</td>
	  <td>Deuteriumia</td>
	</tr>
	<tr>
	  <td>Kaapattu</td>
	  <td><input type="text" name="p<?php echo $pelaaja; ?>_kaapattuMetalli" size="8" value="<?php echo $kaapMet; ?>" /></td>
	  <td><input type="text" name="p<?php echo $pelaaja; ?>_kaapattuKristalli" size="8" value="<?php echo $kaapKrist; ?>" /></td>
	  <td><input type="text" name="p<?php echo $pelaaja; ?>_kaapattuDeuterium" size="8" value="<?php echo $kaapDeut; ?>" /></td>
	</tr>
	<tr>
	  <td>Kierrätetty</td>
	  <td><input type="text" name="p<?php echo $pelaaja; ?>_kierratettyMetalli" size="8" value="<?php echo $kierMet; ?>" /></td>
	  <td><input type="text" name="p<?php echo $pelaaja; ?>_kierratettyKristalli" size="8" value="<?php echo $kierKrist; ?>" /></td>
	  <td></td>
	</tr>
  </table>
  
  <h3>Menetetyt alukset</h3>
  
  <table cellpadding="3">
	<tr>
<?php
		$i=0;
		foreach ($alustenNimet as $lyhenne => $alus){
		  if ($i % 5 == 0 && $i != 0) echo "\t</tr>\n\t<tr>\n";
		  
		  // paattien määrä ensisijaisesti lomakkeesta, toissijaisesti raportinukijalta, viimeisenä vaihtoehtona tyhjä 
		  $lukijalta = ( ( is_object($raportinlukija) && !$raportinlukija->isError() ) ? $raportinlukija->getHajonneetAlukset($pelaaja, $lyhenne) : "" );
		  $paattimaara = ( $_POST["p".$pelaaja."_" . $lyhenne] != "" ? $_POST["p".$pelaaja."_" . $lyhenne] : $lukijalta );
		  
		  echo "\t\t<td><abbr title=\"$alus\">".strtoupper($lyhenne)."</abbr>: ".
			   "<input type=\"text\" name=\"p" . $pelaaja . "_" . $lyhenne."\" size=\"5\" value=\"".$paattimaara."\" /></td>\n";
		  
		  $i++;
		}
	  
?>
	</tr>
  </table>
  
  <hr />
  
  <?php } 
} // else: jos ei olla rapsanlukutoiminnossa
?>
  
  <p>Palautetta vastaanotetaan <a href="http://board.fi.ogame.org/board66-sekalaista/board68-fan-art/4051-lts-tuoton-jakolaskin/" target="_blank">OGame.fi:n boardilla</a><br />
     Copyright &copy; 2009-2012 <a href="http://iki.fi/kuitsi/ogame/">Kuitsi</a>. Released under <a rel="license" href="http://www.gnu.org/licenses/gpl-3.0.txt">GPLv3 license</a>.
     <br />Taustakuva: Copyright &copy; Gameforge Productions GmbH</p>
  
  </div>
</body>
</html>