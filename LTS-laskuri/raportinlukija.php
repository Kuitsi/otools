<?php
/**
 * LTS-tuotonjakolaskuri
 * @author Kuitsi (http://iki.fi/kuitsi)
 * @license GPLv3 (see OTools/LICENSE.txt)
 *
 * Raportinlukija.php lukee taisteluraportista kaiken olennaisen tiedon LTS-tuotonjakolaskuria varten
 * Toimii vain suomenkielisissä raporteissa
 */

class Raportinlukija {
  
  private $error = false;//true jos luettu raportti ollut virheellinen
  
  private $pelaajat = array();// taistelun lopputulos
  // osallistuneiden hyökkääjien määrä = count($pelaajat)
  // jokaiselle pelaajalle oma sisäinen array, jossa
  // [nimi] -> nimi
  // [lyhenne] -> hajonneiden_alusten_määrä

  public function Raportinlukija($raportti){
  
	// avainsanoja/sanontoja taisteluraportista,
	// kaikkiin tulee lisäksi rajoittimiksi alkuun "/" ja loppuun "/i" (i=case insentisive)
	$ATT      = 'Hyökkääjä';
	$DEF      = 'Puolustaja';
	$DESTR    = 'tuhottu';
	$TYPE     = 'Tyyppi(.+)';
	$NUMBER   = 'Yhteensä([\s\d\.]+)';
	$END1     = 'Hyökkääjä on voittanut taistelun\!';
	$END2     = 'Taistelu päättyi tasan, molemmat laivueet palaavat kotiplaneetoilleen.';
	$END3     = 'Puolustaja on voittanut taistelun\!';
	$GAIN     = 'Hän kaappasi';
	$TOP      = 'Seuraavat laivastot kohtasivat taistelussa \(\d{1,2}.*:\d{1,2}\):';//raportin ensimmäinen rivi  
	$ATT_NAME = '(.+)\[\d{1}';//nimen kaappaus sulkeissa (vaatii trim())   Hyökkääjä player_name_here [x:xxx:x]
	$INTER_ROUND = 'Hyökkäävä laivasto tulittaa.+[\n\s]+.+\n';

	// aluksien lyhenteet, samat 2-kirjaimiset lyhenteet kun pääohjelmassakin
	$paatit = array(
	  "P.Rahtialus"   => "pr",
	  "S.Rahtialus"   => "sr",
	  "K.Hävittäjä"   => "kh",
	  "R.Hävittäjä"   => "rh",
	  "Risteilijä"    => "rs",
	  "Taistelualus"  => "ta",
	  "Retk. alus"    => "sa",//siirtokunta-alus
	  "Kierr."        => "kr",
	  "Vak.luotain"   => "vl",
	  "Pommittaja"    => "pm",
	  "Tuhoaja"       => "th",
	  "Kuolemantähti" => "kt",
	  "Taistelurist." => "tr"
	  //TODO: englanninkieliset lyhenteet samaan taulukkoon ja niille suomenkieliset 2-kirjaimiset lyhenteet
	);
  
	// tarkistetaan raportin kelvollisuus
	$alku_ok = preg_match('/'.$TOP.'/i', $raportti);
	$tulos_ok = preg_match('/'.$END1.'/i', $raportti) || preg_match('/'.$END2.'/i', $raportti) || preg_match('/'.$END3.'/i', $raportti);
	$is_valid = $alku_ok && $tulos_ok;
	if (!$is_valid) {
	  echo "<p class=\"error\">Raportti ei ole kelvollinen!</p>\n";
	  $this->error = true;
	}
	else {
	  
	  // millä merkeillä alukset ja niiden määrät on erotettu raporteissa
	  $SEPARATORS = '\s+';//(\t| \t)

	  //poistetaan alusten lyhenteistä välilyönnit, muuten tulee ongelmia splittauksissa.
	  //alaviivat muunnetaan takaisin välilyönneiksi ennen tietojen tallennusta
	  $välinpoistoEtsi = array("Retk. alus");
	  $välinpoistoKorvaa = array("Retk._alus");
	  
	  $kierrokset =  preg_split('/'.$INTER_ROUND.'/i', $raportti);// taistelussa on $kierrokset-1 kierrosta
	  // tarvittavat alusten tiedot löytyy $kierrokset[0] ja $kierrokset[count($kierrokset)-1]
	 
	  preg_match_all('/'.$ATT.$ATT_NAME.'/i', $kierrokset[0], $hyökkääjät);
	  // vaatii vielä trim($hyökkääjät[1][x]) jotta pelaajien nimet olisivat oikein

	  // listataan jokainen hyökkääjä vain kertaalleen:
	  $pelaaja_tmp = array();
	  foreach ($hyökkääjät[1] as $pelaaja) {
		if (!in_array(trim($pelaaja), $pelaaja_tmp)) $pelaaja_tmp[] = trim($pelaaja);
	  }
	  // tallennetaan pelaajien nimet lopulliseen pelaajataulukkoon
	  foreach ($pelaaja_tmp as $pt)
		$this->pelaajat[] = array("nimi" => $pt);
	  
	  // paloitellaan alku- ja lopputilanteet pelaajittain
	  $alku_pelaajat = preg_split('/('.$ATT.'|'.$DEF.') /', $kierrokset[0], -1, PREG_SPLIT_DELIM_CAPTURE);
	  $loppu_pelaajat = preg_split('/('.$ATT.'|'.$DEF.') /', $kierrokset[count($kierrokset)-1], -1, PREG_SPLIT_DELIM_CAPTURE);
	  //echo "<pre>"; print_r($alku_pelaajat); echo "</pre>";
	  
	  // hyökkääjien erilliset laivueet alkutilanteessa
	  $laivueet_alku = array(); $i=0;
	  while ($alku_pelaajat[$i]!=$DEF) {
		if($i%2==0 && $i!=0) $laivueet_alku[] = $alku_pelaajat[$i];
		$i++;
	  }
	  
	  // hyökkääjien erilliset laivueet lopputilanteessa
	  $laivueet_loppu = array(); $i=0;
	  while ($loppu_pelaajat[$i]!=$DEF) {
		if($i%2==0 && $i!=0) $laivueet_loppu[] = $loppu_pelaajat[$i];
		$i++;
	  }

	  // käydään alkutilanne läpi laivueittain ja tallennetaan alusten määrät suoraan oikeille pelaajille
	  $i=0;
	  foreach ($this->pelaajat as $p) {// käydään pelaaja kerran läpi
		foreach ($laivueet_alku as $alku) { // kyseisen pelaajan jokainen laivue
		  if (strpos($alku, $p["nimi"])!==FALSE) { // löyty oikea pelaaja
			preg_match('/'.$TYPE.'/i', $alku, $tyyppi);
			preg_match('/'.$NUMBER.'/i', $alku, $maara);
			
			$tyypit = str_replace($välinpoistoEtsi, $välinpoistoKorvaa, trim($tyyppi[1]));
			$alukset_nimi = preg_split('/'.$SEPARATORS.'/', $tyypit);
			$alukset_lkm  = preg_split('/'.$SEPARATORS.'/', trim($maara[1]));
			$t=0;
			foreach ($alukset_nimi as $nimi) {
			  if (isset($this->pelaajat[$i][$paatit[str_replace("_", " ", $nimi)]]))
				$this->pelaajat[$i][$paatit[str_replace("_", " ", $nimi)]] += str_replace(".", "", $alukset_lkm[$t]);
			  else
				$this->pelaajat[$i][$paatit[str_replace("_", " ", $nimi)]] = str_replace(".", "", $alukset_lkm[$t]);
			  $t++;
			}
		  }
		}
		$i++;
	  }
	  
	  // ...ja sama lopputilanteelle
	  $i=0;
	  foreach ($this->pelaajat as $p) {// käydään pelaaja kerran läpi
		foreach ($laivueet_loppu as $loppu) { // kyseisen pelaajan jokainen laivue
		  if (strpos($loppu, $p["nimi"])!==FALSE) { // löyty oikea pelaaja
			preg_match('/'.$TYPE.'/i', $loppu, $tyyppi);
			preg_match('/'.$NUMBER.'/i', $loppu, $maara);
			
			$tyypit = str_replace($välinpoistoEtsi, $välinpoistoKorvaa, trim($tyyppi[1]));
			$alukset_nimi = preg_split('/'.$SEPARATORS.'/', $tyypit);
			$alukset_lkm  = preg_split('/'.$SEPARATORS.'/', trim($maara[1]));
			$t=0;
			foreach ($alukset_nimi as $nimi) {
			  if (isset($this->pelaajat[$i][$paatit[str_replace("_", " ", $nimi)]]))
				$this->pelaajat[$i][$paatit[str_replace("_", " ", $nimi)]] -= str_replace(".", "", $alukset_lkm[$t]);
			  else
				$this->pelaajat[$i][$paatit[str_replace("_", " ", $nimi)]] = str_replace(".", "", $alukset_lkm[$t]);
			  $t++;
			}
		  }
		}
		$i++;
	  }
	    
	}//else, kelvollinen raportti
	
  }//konstruktori, raportti luettu
  
  /**
   * palauttaa tietyltä pelaajalta pyydetyn alustyypin hajonneiden määrän
   * $lyhenne oltava joku tunnettu 2-kirjaiminen lyhenne
   */
  public function getHajonneetAlukset($pelaaja, $lyhenne){
	return $this->pelaajat[$pelaaja][$lyhenne];
  }
  
  /**
   * palauttaa hyökkäykseen osallistuneiden pelaajien lukumäärän
   */
  public function getPelaajia(){
	return count($this->pelaajat);
  }
  
  /**
   * palauttaa halutun pelaajan nimen
   */
  public function getNimi($pelaaja){
	return $this->pelaajat[$pelaaja]["nimi"];
  }

  /**
   * tarkistaa onko olio luotu kunnolla
   */
  public function isError() {
	return $this->error;
  }

}

?>