<?php
/**
 * LTS-tuotonjakolaskuri
 * @author Kuitsi (http://iki.fi/kuitsi)
 * @license GPLv3 (see OTools/LICENSE.txt)
 *
 * Taistelu.php huolehtii taistelun laajuudella resujen jaosta. Hallinnoi yksittäisiä pelaajia
 */

require("pelaaja.php");
 
class Taistelu {

  private $pelaajat = array(); // taisteluun osallistuvien pelaajien tiedot Pelaaja-olioina
  
  // taistelusta jokaiselle menevä yhtäsuuri osa
  private $metalli;
  private $kristalli;
  private $deuterium;

  public function Taistelu($pelaajia){
	if ($pelaajia < 1 || $pelaajia > 5) { echo "<p class=\"virhe\">Virheellinen määrä pelaajia!</p>\n"; return false; }
	
	for ($i=0; $i<$pelaajia; $i++)
	  $this->pelaajat[$i] = new Pelaaja("Pelaaja ".($i+1));
	  
  }

  /**
   * muuntaa tappiolle jäädyt deutit muiksi resuiksi suhteella 2:1:1
   * $kristalliksi (true/false) kertoo halutaanko ensisijaisesti resut krisuna vai metallina
   */ //polttoainettaKulunut($deuterium)
  public function deuttimuunnos($kristalliksi = false) {
	$pelaajia = count($this->pelaajat);
	$kokMetalli   = 0;
	$kokKristalli = 0;
	$kokDeuterium = 0;
	for ($pelaaja=0; $pelaaja<$pelaajia; $pelaaja++) {
		$p = $this->pelaajat[$pelaaja];
		$kokMetalli   += $p->getMetalli();
		$kokKristalli += $p->getKristalli();
		$kokDeuterium += $p->getDeuterium();
	}
	
	// vain tappiolliset deutit muunnetaan resuiksi
	if ($kokDeuterium < 0) {
		$tappioDeutti = abs($kokDeuterium);
		
		if ($kristalliksi) {
		  // maksimimäärä krisuks, loput metalliks
		  //echo "\n<p><b>degub:</b><br />krisuks $tappioDeutti deuttia<br />";
		  if ($kokKristalli >= $tappioDeutti) {
			// kaikki kristalliksi
			$muuntoKristalli = $tappioDeutti;
			//echo "= $muuntoKristalli kristallia</p>\n";
			// vähennä tappiolliset deutit joltain jolla se on jo miinuksella ja lisää krisut hänelle
		  }
		  else if ($kokKristalli > 0) {
			// deutista kristalliksi niin paljon kuin vaan kristallia on ja loput metalliksi
			$muuntoKristalli = $kokKristalli;
			$muuntoMetalli = 2 * ($tappioDeutti - $kokKristalli);
			
			//echo "= $muuntoKristalli kristallia<br />+ $muuntoMetalli metallia, metallitilanne kokonaisuudessaan ".($kokMetalli-$muuntoMetalli)."</p>\n";
			
			// taas tietojen siirto Pelaajille
		  }
		  else {
			// kaikki metalliks. mielummin metallitappioita ku deuttitappioita
			$muuntoMetalli = 2 * $tappioDeutti;
			
			//echo "= $muuntoMetalli metallia, metallitilanne kokonaisuudessaan ".($kokMetalli-$muuntoMetalli)."</p>\n";
			
			// ja tiedot Pelaaja-olioihin
		  }
		}
		else {
		  // maksimimäärä metalliks, loput krisuks
		  //echo "\n<p><b>debug:</b> metalliks $tappioDeutti deuttia<br />krisun jako koodataan ensin</p>\n";
		}
	  
		//TODO: muuta jokaisen pelaajaolion tietoihin uudet määrät deutille (0) ja muille resuille (>0)  
	}
  }
  
  /**
   * laskee jokaiselle kuuluvan osuuden koko potista,
   * tallentaa tuloksen Taistelu-olion sisäisiin muuttujiin
   * $this->metalli, $this->kristalli ja $this->deuterium
   */
  public function laskeOsuus() {
	$pelaajia = count($this->pelaajat);
	
	// taistelun kokonaisresusaldo
	$kokMetalli = 0;
	$kokKristalli = 0;
	$kokDeuterium = 0;
	for ($pelaaja=0; $pelaaja<$pelaajia; $pelaaja++) {
		$p = $this->pelaajat[$pelaaja];
		$kokMetalli   += $p->getMetalli();
		$kokKristalli += $p->getKristalli();
		$kokDeuterium += $p->getDeuterium();
	}
  
	/* ei noita desimaalilukuja jaksa kukaan katella, virhe kuitenkin
	   maksimissaan (pelaajienMäärä-1) yksikköä */
	$this->metalli   = floor($kokMetalli   / $pelaajia);
	$this->kristalli = floor($kokKristalli / $pelaajia);
	$this->deuterium = floor($kokDeuterium / $pelaajia);
	
	// palautetaan vielä taulukkona jotta voidaan näyttää helposti tuloksissa
	$osuus["met"] = $this->metalli;
	$osuus["krist"] = $this->kristalli;
	$osuus["deut"] = $this->deuterium;
	return $osuus;
  }
  
  /**
   * palauttaa taulukossa jokaisen pelaajan tulevat ja menevät resut
   * kunhan laskeOsuus() on ensin kutsuttu
   */
  public function haeTulos() {
	$pelaajia = count($this->pelaajat);
	for ($pelaaja=0; $pelaaja<$pelaajia; $pelaaja++) {
		$p = $this->pelaajat[$pelaaja];
		$pMet   = $p->getMetalli();
		$pKrist = $p->getKristalli();
		$pDeut  = $p->getDeuterium();
		
		$tulos[$pelaaja]["met"]   = 0 - ($pMet   - $this->metalli);
		$tulos[$pelaaja]["krist"] = 0 - ($pKrist - $this->kristalli);
		$tulos[$pelaaja]["deut"]  = 0 - ($pDeut  - $this->deuterium);
		
	}
	return $tulos;
  }
  
  /**
   * palauttaa halutun pelaajan (indeksit 0 - pelaajien_määrä-1) olion
   * jos oliota ei löydy tai indeksi ei ole kelvollinen, ilmoitetaan virheestä
   */
  public function getPelaaja($numero) {
	if ($numero < 0 || $numero > count($this->pelaajat)-1) { echo "<p class=\"virhe\">Virheellinen pelaajan indeksi!</p>\n"; return false; }
	if ($this->pelaajat[$numero] == null) { echo "<p class=\"virhe\">Haettu Pelaaja-olio ei ole olemassa</p>\n"; return false; }
	
	return $this->pelaajat[$numero];
  }

}

?>