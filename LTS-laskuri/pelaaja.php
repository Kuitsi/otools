<?php
/**
 * LTS-tuotonjakolaskuri
 * @author Kuitsi (http://iki.fi/kuitsi)
 * @license GPLv3 (see OTools/LICENSE.txt)
 *
 * Pelaaja.php huolehtii yksittäisen pelaajan tiedoista taistelussa
 */

class Pelaaja {

  private $nimi; // pelaajan nimi, helpottaa tulosten näyttämistä
  
  // resusaldo: negatiiviset arvot tappioita, positiiviset voittoja
  private $metalli;
  private $kristalli;
  private $deuterium;

  // konstruktori
  public function Pelaaja($nimi){
	$this->nimi = $nimi;
	$this->metalli = 0;
	$this->kristalli = 0;
	$this->deuterium = 0;
  }
  
  public function setNimi($nimi) {
	if ($nimi != "") $this->nimi = $nimi;
	else return false;
  }

  /**
  * lisää tuhoutuneiden alusten hinnat tappioihin
  * $alukset on array('lyhenne' => lukumäärä)
  */
  public function aluksiaMenetetty($alukset) {
	if (!is_array($alukset)) { echo "<p class=\"virhe\">Virhe: Menetetyt alukset pitää ilmoittaa taulukossa!</p>\n"; return false; }
	
	// Pieni rahtialus	
	$this->metalli   -= $alukset['pr']*2000;
	$this->kristalli -= $alukset['pr']*2000;
		
	// Suuri rahtialus
	$this->metalli   -= $alukset['sr']*6000;
	$this->kristalli -= $alukset['sr']*6000;
		
	// Kevyt hävittäjä
	$this->metalli   -= $alukset['kh']*3000;
	$this->kristalli -= $alukset['kh']*1000;
		
	// Raskas hävittäjä
	$this->metalli   -= $alukset['rh']*6000;
	$this->kristalli -= $alukset['rh']*4000;
		
	// Risteilijä
	$this->metalli   -= $alukset['rs']*20000;
	$this->kristalli -= $alukset['rs']*7000;
	$this->deuterium -= $alukset['rs']*2000;
		
	// Taistelualus
	$this->metalli   -= $alukset['ta']*45000;
	$this->kristalli -= $alukset['ta']*15000;
		
	// Siirtokunta-alus
	$this->metalli   -= $alukset['sa']*10000;
	$this->kristalli -= $alukset['sa']*20000;
	$this->deuterium -= $alukset['sa']*10000;
		
	// Kierrättäjä
	$this->metalli   -= $alukset['kr']*10000;
	$this->kristalli -= $alukset['kr']*6000;
	$this->deuterium -= $alukset['kr']*2000;
		
	// Vakoiluluotain
	$this->kristalli -= $alukset['vl']*1000;
		
	// Pommittaja
	$this->metalli   -= $alukset['pm']*50000;
	$this->kristalli -= $alukset['pm']*25000;
	$this->deuterium -= $alukset['pm']*15000;
		
	// Tuhoaja
	$this->metalli   -= $alukset['th']*60000;
	$this->kristalli -= $alukset['th']*50000;
	$this->deuterium -= $alukset['th']*15000;
		
	// Kuolemantähti
	$this->metalli   -= $alukset['kt']*5000000;
	$this->kristalli -= $alukset['kt']*4000000;
	$this->deuterium -= $alukset['kt']*1000000;
		
	// Taisteluristeilijä
	$this->metalli   -= $alukset['tr']*30000;
	$this->kristalli -= $alukset['tr']*40000;
	$this->deuterium -= $alukset['tr']*15000;
  }

	/**
	 * lisää ammuttujen ohjusten hinnan tappioihin
	 * $lkm = ammuttujen ohjusten määrä
	 */
	public function ohjuksiaAmmuttu($lkm) {
	  if ($lkm < 0) { echo "<p class=\"virhe\">Virhe: Ohjusten lukumäärä ei ole kelvollinen!</p>\n"; return false; }
	  
	  $this->metalli   -= $lkm*12500;
	  $this->kristalli -= $lkm*2500;
	  $this->deuterium -= $lkm*10000;
	}

	/**
	 * lisää kuluneen polttoaineen määrän tappioihin
	 * $deuterium = kaikkien laivueiden kokonaiskulutus
	 */
	public function polttoainettaKulunut($deuterium) {
		$this->deuterium -= $deuterium;
	}

	/**
	 * vähentää tappioista kaapattujen resurssien määrän
	 * $metallia    = kaapattu metalli
	 * $kristallia  = kaapattu kristalli
	 * $deuteriumia = kaapattu deuterium
	 */
	public function kaapattuMukaan($metallia, $kristallia, $deuteriumia) {
		$this->metalli   += $metallia;
		$this->kristalli += $kristallia;
		$this->deuterium += $deuteriumia;
	}

	/**
	 * tallentaa pelaajan kierrättämän osuuden ja
	 * lisäksi vähentää kierrätetyn osuuden tappioista
	 * $metallia ja $kristallia on kierrätetyt määrät
	 */
	public function kierratetty($metallia, $kristallia) {
		$this->metalli   += $metallia;
		$this->kristalli += $kristallia;
	}
	
	/**
	 * lukufunktiot
	 */
	public function getNimi()      { return $this->nimi; }
	public function getMetalli()   { return $this->metalli; }
	public function getKristalli() { return $this->kristalli; }
	public function getDeuterium() { return $this->deuterium; }

}

?>