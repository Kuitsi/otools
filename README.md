# OTools

OTools is a collection of online tools for browser game [OGame][6]. These are made mainly for Finnish server
but these should work also in ogame.org server. User interface is in Finnish though.


## License

    Copyright (C) 2009-2013  Juha Kuitunen

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program. If not, see <http://www.gnu.org/licenses/>.


### Note

I'm not actively developing these anymore but you can still send me a pull request
if you have something useful to add here. :)


## [Konvertteri][1]

Converts battle reports to BBCode for use in forums. It supports battle reports from .fi and .org servers.
Finnish feedback thread is located [here][2].

Konvertteri has been the most used battle report converter in Finnish OGame community for last few years. \o/


## [LTS-laskuri][3]

Split profits and costs of ACS Attack between participants.
Finnish feedback thread is located [here][4].


## [Aikalaskureita][5]

`ogame-laskureita.php` contains some simple flight time calculators


[1]: http://iki.fi/kuitsi/ogame/konvertteri/
[2]: http://board.fi.ogame.org/board66-sekalaista/board68-fan-art/5272-konvertteri/
[3]: http://iki.fi/kuitsi/ogame/lts-laskuri/
[4]: http://board.fi.ogame.org/board66-sekalaista/board68-fan-art/4051-lts-tuoton-jakolaskin/
[5]: http://iki.fi/kuitsi/ogame/ogame-laskureita.php
[6]: http://fi.ogame.org/