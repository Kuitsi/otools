<?php
/**
 * Konvertterin raportinlukija
 * @author Kuitsi (http://iki.fi/kuitsi)
 * @license GPLv3 (see OTools/LICENSE.txt)
 *
 * Lukee OGamen taisteluraportteja ja kerää niistä tarvittavat tiedot boardille konvertointia varten
 *
 * Olion alustus: $r = new Raportinlukija( String $raportti [, boolean $olenhyökkääjä [, String $kieli [, float $defurepair ] ] ] );
 * Katso lukufunktiot tiedoston lopusta
 */

// rajoitetaan PHP:n virheraportointia: E_NOTICE herjaa kun puolustaja voittaa eikä hyökkääjä saakaan kaapattua resoja
//error_reporting(E_ERROR | E_WARNING | E_PARSE);

class Raportinlukija {

/**
 * Sisäiset muuttujat lopputulosta varten
 */
private $alku_att = array();    // hyökkääjät alussa
private $alku_def = array();    // puolustajat alussa
private $loppu_att = array();   // hyökkääjät lopussa
private $loppu_def = array();   // puolustajat lopussa
private $tulos = "";            // taisteluraportin loppuosa
private $kierroksia = 0;        // taistelun kesto
private $aika = "";             // taistelun ajankohta
private $kieli = "";            // taisteluraportin kieli
private $attTuotot = 0;         // hyökkääjän hyödyllisyys
private $defTuotot = 0;         // puolustajan hyödyllisyys
private $defRealTuotot = 0;     // puolustajan hyödyllisyys, kun arvioidaan defujen korjautuminen taistelun jälkeen
private $kokoNimet = array();   // alusten ja defujen lyhenteet ja niitä vastaavat nimet, oikealla kielellä
private $defuLyh = array();     // defujen lyhenteet oikealla kielellä
private $cost = array();        // alusten ja defujen hinnat
private $romukenttä = array();  // RK:n koko [0]=metalli, [1]=kristalli
private $kierrätetty = array(); // RK:sta kierrätetty yhteensä [0]=metalli, [1]=kristalli
private $kaapattu = array();    // kaapatut resut [0]=metalli, [1]=kristalli, [2]=deuterium
private $koordinaatit = "";     // taistelun koordinaatit (esim polttoaineen kulutusta varten)
//private $defulista = array();
//private $aluslista = array();
private $hajonneet_defut = array();
private $korjatut_defut = array();
private $defKorjatut = 0;       // puolustajan korjautuneiden defujen arvo
private $defKorjatutIns = NULL;

/**
 * Konstruktori
 * parametreina luettu raportti ($raportti), $olenhyökkääjä kertoo onko konvertoija hyökkääjä,
 * $lang on raportissa käytetty kieli (fi, en, auto = automaattinen tunnistus)
 * $defurepair on puolustuslaitteiden korjauskerroin taistelun jälkeen (0.7 normaaleissa universumeissa)
 */
public function Raportinlukija($raportti, $olenhyökkääjä=1, $lang="auto", $defurepair=0.7){

  // selvitetään raportin kieli automaattisesti
  if ($lang="auto") {
	if     (preg_match("/Attacker/i",$raportti))  $lang = "en";
	elseif (preg_match("/Hyökkääjä/i",$raportti)) $lang = "fi";
	else $lang = "fi";// yritetään suomea jos muuta ei löydy
  }
  // haetaan oikea kielitiedosto
  require("lang_".$lang.".php");

  $this->kieli = $lang;

  // tarkistetaan raportin kelvollisuus
  $alku_ok = preg_match('/'.$TOP.'/i', $raportti);
  $tulos_ok = preg_match('/'.$END1.'/i', $raportti) || preg_match('/'.$END2.'/i', $raportti) || preg_match('/'.$END3.'/i', $raportti);
  $is_valid = $alku_ok && $tulos_ok;
  if (!$is_valid) die("Raportti ei ole kelvollinen!\n");

  // millä merkeillä alukset ja niiden määrät on erotettu raporteissa (" \t" = redesign)
  $SEPARATORS = '\s+';

  // haetaan yksikköjen nimet oikeasta kielitiedostosta ja muunnetaan lyhenteet pieniksi kirjaimiksi, jotta replace onnistuu varmemmin
  foreach($lyhenteet[$lang] as $key => $value) {
	$this->kokoNimet[strtolower($key)] = $value;
  }
  reset($this->kokoNimet);//nollataan pointteri osoittamaan takaisin taulukon alkuun

  // kerätään defut ja alukset eri taulukoihin hyödyllisyyden laskua varten
/*  $nimitemp=0;
  foreach ($this->kokoNimet as $key => $value) {
	if ($nimitemp<8)
	  $this->defulista[] = $key;
	else
	  $this->aluslista[] = $key;
	$nimitemp++;
  }
  reset($this->kokoNimet);*/

  for($l=0; $l<8; $l++) {//defut omaan taulukkoon, jotta voidaan laskea hajonneista tykeistä oikea %-osuus hyödyllisyyteen
	$defutaulu[current($this->kokoNimet)] = key($this->kokoNimet);
	next($this->kokoNimet);
  }
  $this->defuLyh = $defutaulu;

  // alusten hinnat oikean kielisillä lyhenteillä varustettuna taulukkoon
  // ekana tykit, sitten paatit
  //             RH,   KL,   RL,   GT,    IT,   PT,    PS,    SS,    PR,   SR,   KH,   RH,   RS,    TA,    SA,    KR,    VL,   PM,    AS,   TH,    KT,      TR 
  $met   = array(2000, 1500, 6000, 20000, 2000, 50000, 10000, 50000, 2000, 6000, 3000, 6000, 20000, 45000, 10000, 10000, 0,    50000, 0,    60000, 5000000, 30000);
  $krist = array(0,    500,  2000, 15000, 6000, 50000, 10000, 50000, 2000, 6000, 1000, 4000, 7000,  15000, 20000, 6000,  1000, 25000, 2000, 50000, 4000000, 40000);
  $deut  = array(0,    0,    0,    2000,  0,    30000, 0,     0,     0,    0,    0,    0,    2000,  0,     10000, 2000,  0,    15000, 500,  15000, 1000000, 15000);
  foreach($this->kokoNimet as $lyh => $nimi){
	$units[] = $lyh;
  }
  for($p=0;$p<count($units);$p++){
	$this->cost[$units[$p]][0] = $met[$p];
	$this->cost[$units[$p]][1] = $krist[$p];
	$this->cost[$units[$p]][2] = $deut[$p];
  }
  //echo "<pre>"; print_r($this->cost); echo "</pre>\n";//hintojen tarkistus

  /**
   * asetukset loppuu
   **************************************************************************************************
   */

  $kierrokset =  preg_split('/'.$INTER_ROUND.'/i', $raportti);

  // taistelun kesto
  $this->kierroksia = count($kierrokset)-1;

  // varmistetaan oikeanlainen parsiminen myös drive-through -hyökkäyksissä (puolustajalla ei yhtään mitään)
  if ($this->kierroksia == 0) {
	$kierrokset = preg_split('/('.$END1.'|'.$END2.'|'.$END3.')/i', $raportti);
	$alku_pelaajat = preg_split('/('.$ATT.'|'.$DEF.') /i', $kierrokset[0], -1, PREG_SPLIT_DELIM_CAPTURE);
	$loppu_pelaajat = preg_split('/('.$ATT.'|'.$DEF.') /i', $kierrokset[0], -1, PREG_SPLIT_DELIM_CAPTURE);
  }
  else {
	$alku_pelaajat = preg_split('/('.$ATT.'|'.$DEF.') /i', $kierrokset[0], -1, PREG_SPLIT_DELIM_CAPTURE);
	$loppu_pelaajat = preg_split('/('.$ATT.'|'.$DEF.') /i', $kierrokset[count($kierrokset)-1], -1, PREG_SPLIT_DELIM_CAPTURE);
  }

  /**
   * alkutilanne
   */

  $i=0;
  //echo "alkutilanne<br />\n";
  foreach ($alku_pelaajat as $alku) {
	if ($i%2==0 && $i!=0) {
	  //$alku-muuttujassa nyt yksittäinen laivue taistelun alussa
	
	  $alku = trim($alku);
	  unset($tmp);

	  // tarkastetaan onko pelaaja hyökkääjä vai puolustaja
	  if ($alku_pelaajat[$i-1]==$ATT) {
		$hyökkääjä = 1;
	  }
	  else /* $alku_pelaajat[$i-1]==$DEF */ {
		$hyökkääjä = 0;
	  }

	  preg_match('/'.$TECHS.'/i', $alku, $tekit);
	  preg_match('/'.$ATT_NAME_COORDS.'/i', $alku, $namecoords);
	  preg_match('/'.$TYPE.'/i', $alku, $tyyppi);
	  preg_match('/'.$NUMBER.'/i', $alku, $maara);
		
	  $tmp["nimi"] = trim($namecoords[1]);
	  $tmp["koordinaatit"] = $namecoords[2];
	  $tmp["tekit"] = $tekit[0];
	
	  // alusten (ja defujen) tyypit sekä määrät
	  $tyypit = mb_strtolower(str_ireplace($välinpoistoEtsi, $välinpoistoKorvaa, trim($tyyppi[1])), 'UTF-8');
	  $yksiköt_nimi = preg_split('/'.$SEPARATORS.'/', $tyypit);
	  $yksiköt_lkm  = preg_split('/'.$SEPARATORS.'/', trim($maara[1]));

	  $t=0;
	  foreach ($yksiköt_nimi as $nimi) {
		if (preg_match('/'.$DESTR.'/i', $alku)) $tmp["tuhottu"] = 1;
		else {
		  $tmp[$nimi] = str_replace(".", "", $yksiköt_lkm[$t]);
		  //Lasketaan kaikki yksiköt ensin tappioiksi, ehjänä säilyneet vähennetään lopputilanteen yhteydessä.
		  //Tämä on helpompaa näin, koska raportin lopputilanteessa ei näy täysin hajonneita alustyyppejä
		  $this->calcLosses($nimi, str_replace(".", "", $yksiköt_lkm[$t]), $hyökkääjä);
		  //echo $nimi.": ".$yksiköt_lkm[$t].", kerroin: ".$kerroin.", hyökkääjä: ".$hyökkääjä."<br />\n";
		}
		$t++;
	  }
	
	  if ($hyökkääjä) $this->alku_att[] = $tmp;
	  else /*if (!$hyökkääjä) */ $this->alku_def[] = $tmp;
	}
	$i++;
  }

  // laivueiden määrä yhteensä (hyökkääjät+puolustajat), helpottaa lopputilanteen parsimista
  $pelaajia = count($this->alku_att) + count($this->alku_def);

  /**
   * lopputilanne
   */

  $i=0;
  //echo "lopputilanne<br />\n";
  foreach ($loppu_pelaajat as $loppu) {
	if ($i%2==0 && $i!=0) {
	  //$loppu-muuttujassa nyt yksittäinen laivue taistelun lopussa

	  // siistitään vähän ja nollataan temppimuuttuja (tulee muuten ongelmia)
	  $loppu = trim($loppu);
	  unset($tmp);

	  // tarkastetaan onko pelaaja hyökkääjä vai puolustaja
	  if ($alku_pelaajat[$i-1]==$ATT) {
		$hyökkääjä = 1;
	  }
	  else /* $alku_pelaajat[$i-1]==$DEF */ {
		$hyökkääjä = 0;
	  }

	  //käsitellään täysin tuhoutunut laivue erikseen
	  if(preg_match('/'.$ATTDEF_DESTR.'/i', $loppu, $poksahtanu)) {
		//echo "<pre>$i: $loppu</pre>\n";
		$tmp["nimi"] = trim($poksahtanu[1]);
		$tmp["tuhottu"] = 1;
	  }
	  else {
		preg_match('/'.$TECHS.'/i', $loppu, $tekit);
		preg_match('/'.$ATT_NAME_COORDS.'/i', $loppu, $namecoords);
		preg_match('/'.$TYPE.'/i', $loppu, $tyyppi);
		preg_match('/'.$NUMBER.'/i', $loppu, $maara);

		$tmp["nimi"] = trim($namecoords[1]);
		$tmp["koordinaatit"] = $namecoords[2];

		// alusten (ja defujen) tyypit sekä määrät
		$tyypit = mb_strtolower(str_ireplace($välinpoistoEtsi, $välinpoistoKorvaa, trim($tyyppi[1])), 'UTF-8');
		$yksiköt_nimi = preg_split('/'.$SEPARATORS.'/', $tyypit);
		$yksiköt_lkm  = preg_split('/'.$SEPARATORS.'/', trim($maara[1]));
		$t=0;
		foreach ($yksiköt_nimi as $nimi) {
		  if (preg_match('/'.$DESTR.'/i', $loppu)) $tmp["tuhottu"] = 1;
		  else {
			$tmp[$nimi] = str_replace(".", "", $yksiköt_lkm[$t]);

			// vähennetään ehjänä säilyneiden alusten ja defujen hinnat tappioista (määrän kerroin -1).
			// hajonneiden defujen korjautuminen käsitellään myöhemmin koodissa omassa osiossaan
			$this->calcLosses($nimi, -1 * str_replace(".", "", $yksiköt_lkm[$t]), $hyökkääjä);
		  }
		  $t++;
		}
	  }

	  if ($hyökkääjä) $this->loppu_att[] = $tmp;
	  else /*if (!$hyökkääjä) */ $this->loppu_def[] = $tmp;
	}
	$i++;

	// ei parsita turhaan kaikkea ylimääräistä sälää
	if (count($this->loppu_att) + count($this->loppu_def) >= $pelaajia) break;
  }

  /**
   * lopputuloksen parsiminen suoraan alkuperäisestä raportista (helpompaa kuin splitatusta rapsasta)
   */
  $lopputulos = preg_split('/('.$END1.'|'.$END2.'|'.$END3.')/i', $raportti, -1, PREG_SPLIT_DELIM_CAPTURE);
  //$lopputulos[1] // kuka voitti vai tuliko tasapeli
  //$lopputulos[2] // loput numeeriset sälät
  $lopputulos[2] = preg_replace("/[\r\n ]*\[\d+:\d+:\d+\],/i", "", $lopputulos[2]);//poistetaan koordinaatit
  $this->tulos = $lopputulos[1]."\n\n".trim($lopputulos[2]);

  /**
   * taistelun ajankohta raportin alusta
   */
  preg_match('/'.$TOP.'/i', $raportti, $ajankohta);
  $this->aika = $ajankohta[0];

  /**
   * kaapatut resut
   */
  if (preg_match_all('/'.$STEALED.'/i', $raportti, $kaapattu, PREG_SET_ORDER) > 0) {
	$kaapmet   = 0;
	$kaapkrist = 0;
	$kaapdeut  = 0;
	foreach ($kaapattu as $k) {
	  $kaapmet   += str_replace(".", "", $k[2]);
	  $kaapkrist += str_replace(".", "", $k[4]);
	  $kaapdeut  += str_replace(".", "", $k[6]);
	}
	$this->kaapattu[0] = $kaapmet;
	$this->kaapattu[1] = $kaapkrist;
	$this->kaapattu[2] = $kaapdeut;
	$this->attTuotot += $this->kaapattu[0] + $this->kaapattu[1] + $this->kaapattu[2];
	$this->defTuotot -= $this->kaapattu[0] - $this->kaapattu[1] - $this->kaapattu[2];
  }

  /**
   * syntynyt rk
   * TODO: aallot (preg_match_all, ks kierrätysrapsat)
   */
  preg_match('/'.$DEBRIS.'/i', $raportti, $rk);
  $this->romukenttä[0] = str_replace(".", "", $rk[1]); //metalli
  $this->romukenttä[1] = str_replace(".", "", $rk[2]); //kristalli


  /**
   * puolustajalta taistelussa hajonneet defut. osa näistä korjautuu: selvitä korjautuneet puolustajan rapsasta tai arvioi hyökkääjän rapsasta
   */
  foreach ($this->alku_def[0] as $key => $value) {
	if ( in_array($key, $this->defuLyh) )
	  $this->hajonneet_defut[$key] = $value;
  }
  foreach ($this->loppu_def[0] as $key => $value) {
	if ( in_array($key, $this->defuLyh) )
	  $this->hajonneet_defut[$key] -= $value;
  }
  if (!$olenhyökkääjä) {
	// Puolustajan rapsa: varmasti korjaantumeet defut, voidaan laskea suoraan hyödyllisyys
	if ( preg_match('/'.$REPAIRED_REGEXP.$REPAIRED.'/i', $this->tulos, $korjatut_defut) > 0 ) {
	  $defutemppi = str_replace($REPAIRED, "", $korjatut_defut[0]);
	  $defutemppi = explode(', ', $defutemppi);
	  $def_lyhenteet = $this->defuLyh;
	  foreach ($defutemppi as $dt) {
		list($value, $key) = explode(' ', $dt, 2);
		$value = str_replace('.', '', $value);
		$key = $def_lyhenteet[$key];
		$this->korjatut_defut[$key] = $value;
	  }
	//	echo "<pre>puolustajalta varmasti korjatut defut taistelun jälkeen (\"...yksikköä voitiin korjata\"):\n"; print_r($this->korjatut_defut); echo "</pre>";
	}
  }
  else {
	// Hyökkääjän rapsa: arvioidaan defujen korjautuminen insinöörin kanssa ja ilman
	$defurepair_insinööri = $defurepair + (1-$defurepair)/2; // ==0.85, jos $defurepair==0.7
    //echo "Korjauskertoimet: $defurepair : $defurepair_insinööri<br />\n";


//TODO: korjautuneet defut talletetaan muualle ja niiden hyödyllisyys lasketaan erikseen
//if (in_array($nimi, $this->aluslista))
//if (in_array($nimi, $this->defulista))
  }



  /**
   * kierrätysrapsat
   */
  if (preg_match_all('/'.$RECYCLED.'/i', $raportti, $kierrätetty, PREG_SET_ORDER) > 0) {
	$kiermet = 0;
	$kierkrist = 0;
	foreach ($kierrätetty as $kierr) {
	  $kiermet   += str_replace(".", "", $kierr[3]);
	  $kierkrist += str_replace(".", "", $kierr[4]);
	}
	$this->kierrätetty[0] = $kiermet;
	$this->kierrätetty[1] = $kierkrist;

	if ($olenhyökkääjä) {//hyökkääjälle rk
	  $this->attTuotot += $this->kierrätetty[0] + $this->kierrätetty[1];
	}
	else {//puolustajalle rk
	  $this->defTuotot += $this->kierrätetty[0] + $this->kierrätetty[1];
	}
  }

  /**
   * Taistelun koordinaatit on samat kuin ensimmäisen puolustajan koordinaatit
   */
  $this->koordinaatit = str_replace(array("[", "]"), "", $this->alku_def[0]["koordinaatit"]);

}//konstruktori

/**
 * Lisää menetetyt alukset/defut tappioiksi
 * $yksikkö on aluksen/defun lyhenne rapsasta
 * $määrä on hajonneiden lkm
 * $hyökkääjä (true/false)
 */
private function calcLosses($yksikkö, $määrä, $hyökkääjä){

  $hajonnut = $määrä*($this->cost[$yksikkö][0] + $this->cost[$yksikkö][1] + $this->cost[$yksikkö][2]);
  //echo "<p>$hyökkääjä: $hajonnut (määrä: $määrä, yksikkö: $yksikkö)</p>\n";
  if ($hyökkääjä) {
	$this->attTuotot -= $hajonnut;
  }
  else {
	$this->defTuotot -= $hajonnut;
  }
}
/**
 * Laskee puolustajan todellisen hyödyllisyyden defujen korjautumisen jälkeen
 * $yksikkö on aluksen/defun lyhenne rapsasta
 * $määrä on hajonneiden lkm
 * $hyökkääjä (true/false)
 */
private function calcDefRepaired($yksikkö, $määrä, $hyökkääjä, $defurepair){

  $hajonnut = $määrä*($this->cost[$yksikkö][0] + $this->cost[$yksikkö][1] + $this->cost[$yksikkö][2]);
  $this->defKorjatut += $hajonnut;
}

/**
 * Lukufunktiot
 */
public function getAlkuAtt()        { return $this->alku_att; }       //hyökkääjät alkutilanteessa, taulukoituna
public function getAlkuDef()        { return $this->alku_def; }       //puolustajat alkutilanteessa, taulukoituna
public function getLoppuAtt()       { return $this->loppu_att; }      //hyökkääjät lopputilanteessa, taulukoituna
public function getLoppuDef()       { return $this->loppu_def; }      //puolustajat lopputilanteessa, taulukoituna
public function getLoppurapsa()     { return $this->tulos; }          //raportin loppuosa (kuka voitti, mitä hajosi, mitä kaapattu yms)
public function getKierrokset()     { return $this->kierroksia; }     //taistelun kesto kierroksina
public function getAika()           { return $this->aika; }           //taistelun ajankohta (ensimmäinen rivi)
public function getKieli()          { return $this->kieli; }          //raportin kieli
public function getLyhenteet()      { return $this->kokoNimet; }      //alusten ja puolustusten lyhenteet=>koko_nimet taulukoituna raportissa olleella kielellä
public function getAttTuotot()      { return $this->attTuotot; }      //hyökkääjän hyödyllisyys
public function getDefTuotot()      { return $this->defTuotot; }      //puolustajan hyödyllisyys
public function getDefKorjatut()    { return $this->defKorjatut; }    //puolustajan korjautuneiden defujen arvo
public function getDefKorjatutIns() { return $this->defKorjatutIns; } //puolustajan korjautuneiden defujen arvo arvioituna insinöörin kanssa
public function getRK()             { return $this->romukenttä; }     //muodostunut romukenttä, taulukossa metalli ja kristalli
public function getKierrätetty()    { return $this->kierrätetty; }    //romukentästä kierrätetty yhteensä, taulukossa metalli ja kristalli
public function getDefuLyh()        { return $this->defuLyh; }        //puolustusten koko nimet ja lyhenteet käytetyllä kielellä, taulukoituna
public function getKoordinaatit()   { return $this->koordinaatit; }   //taistelun koordinaatit

}//class
?>