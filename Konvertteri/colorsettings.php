<?php
/**
 * Konvertterin väriasetukset
 * @author Kuitsi (http://iki.fi/kuitsi)
 * @license GPLv3 (see OTools/LICENSE.txt)
 */

$vari["oma"] = "#66cc66";
$vari["vihu"] = "#ff6666";
$vari["ehjänä"] = "#30ff30";
$vari["tappiolla"] = "#ff3030";
$vari["hajonnut"] = "#ff0000";//hajonneiden yksiköiden lkm sulkeissa
$vari["hyödyllisyys"] = "#ffff00";
$vari["kaappasi"] = "#ffff00";//hän kaappasi: resujen määrät
$vari["hyöd_RK"] = $vari["hyödyllisyys"];
$vari["kuu"] = "#ffff00";
$vari["RK"] = "#bbbb00";
//liukuvärit
$vari["att1"] = "#00ff00";
$vari["att2"] = "#0066ff";
$vari["def1"] = "#ff0000";
$vari["def2"] = "#ffff00";
$vari["nimi_default"] = "#00a1ff";
$vari["liittouma_default"] = "#ffff00";
$vari["liittouma"] = $vari["liittouma_default"];// voidaan määritellä index.php:ssa käyttäjän toimesta
$vari["nimi"] = $vari["nimi_default"];//  voidaan määritellä index.php:ssa käyttäjän toimesta
?>