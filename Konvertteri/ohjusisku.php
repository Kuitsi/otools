<?php
/**
 * Konvertterin raportinlukija ohjusiskun vakoiluraportteja varten
 * @author Kuitsi (http://iki.fi/kuitsi)
 * @license GPLv3 (see OTools/LICENSE.txt)
 */

class Ohjusisku {

  /**
  * Sisäiset muuttujat lopputulosta varten
  */
  private $alku_aika = "";
  private $loppu_aika = "";
  private $alku_defut = array();
  private $loppu_defut = array();
  private $hajonneet = array();   // hajonneiden defujen (ja ohjusten) lukumäärät
  private $defujenHinta = 0;      // puolustajalta tuhoutuneiden defujen hinta
  private $ohjustenLkm = 0;       // ammuttujen ohjusten määrä
  private $ohjustenHinta = 0;     // ammuttujen ohjusten hinta
  private $cost = array();        // defujen ja ohjusten hinnat

  /**
  * Konstruktori
  * parametreina alku- ja lopputilanteen vakoiluraportit sekä ammuttujen ohjusten määrä (int)
  * $lang=vakoiluraportin kieli
  */
  public function Ohjusisku($alkutilanne, $lopputilanne, $ohjuksia, $lang="auto"){
  
	  // selvitetään raportin kieli automaattisesti
	if ($lang="auto") {
	  if      (preg_match("/Defense/i",$alkutilanne))  $lang = "en";
	  else if (preg_match("/Puolustus/i",$alkutilanne)) $lang = "fi";
	  else $lang = "fi";// yritetään suomea jos muuta ei löydy
	}
	// haetaan oikea kielitiedosto
	require("lang_".$lang.".php");

	/**
	 * defujen hinnat
	 * lyhenteillä varustettuna taulukkoon, sama järjestys kuin käännöstiedostossakin
	 */
	//             RH,   KL,   RL,   GT,    IT,   PT,    PS,    SS,    TO,   PO
	$met   = array(2000, 1500, 6000, 20000, 2000, 50000, 10000, 50000, 8000, 12500 );
	$krist = array(0,    500,  2000, 15000, 6000, 50000, 10000, 50000, 0,    2500  );
	$deut  = array(0,    0,    0,    2000,  0,    30000, 0,     0,     2000, 10000 );
	foreach($DEFUNIMET as $lyh => $nimi){
	  $units[] = $lyh;
	}
	for($p=0;$p<count($units);$p++){
	  $this->cost[$units[$p]][0] = $met[$p];
	  $this->cost[$units[$p]][1] = $krist[$p];
	  $this->cost[$units[$p]][2] = $deut[$p];
	}
  
	/**
	 * vakoiluraporttien otsikkorivit
	 * PHP < 5.3.0: http://www.php.net/manual/en/info.configuration.php#ini.magic-quotes-gpc
	 */
	if (get_magic_quotes_gpc()) {
	  $alkutilanne = stripslashes($alkutilanne);
	  $lopputilanne = stripslashes($lopputilanne);
	}
	preg_match('/'.$vakoiluOtsikko.'/i', $alkutilanne, $matches_alku);
	$this->alku_aika = $matches_alku[4];
	preg_match('/'.$vakoiluOtsikko.'/i', $lopputilanne, $matches_loppu);
	$this->loppu_aika = $matches_loppu[4];
	
	// pieni tarkistus kannattaako jatkaa eteenpäin
	if ( $matches_alku[2] != $matches_loppu[2] ) die("Raporttien koordinaatit eivät täsmää!");

	$this->ohjustenLkm = $ohjuksia;

	/**
	 * alku- ja lopputilanteen defut taulukoihin
	 */
	foreach($DEFUNIMET as $key => $value) {
		if (preg_match('/'.$value.'\s+([\d\.]+)/i', $alkutilanne, $matches))
		  $this->alku_defut[$key] = str_replace(".", "", $matches[1]);
		  
		if (preg_match('/'.$value.'\s+([\d\.]+)/i', $lopputilanne, $matches))
		  $this->loppu_defut[$key] = str_replace(".", "", $matches[1]);
	}
	
	/**
	 * ammuttujen ohjusten hinta
	 */
	 $this->ohjustenHinta = $ohjuksia * ($this->cost["PO"][0] + $this->cost["PO"][1] + $this->cost["PO"][2]);
	 
	/**
	 * hajonneiden defujen hinta
	 */
	foreach ($this->alku_defut as $key => $value) {
	  $tmp = ( array_key_exists($key, $this->loppu_defut) ? $this->loppu_defut[$key] : 0 );
	  $this->hajonneet[$key] = $this->alku_defut[$key] - $tmp;
	  $this->defujenHinta += $this->hajonneet[$key] * ($this->cost[$key][0] + $this->cost[$key][1] + $this->cost[$key][2]);
	}
	
  } // konstruktori

  /**
   * lukufunktiot
   */
  public function getOhjustenHinta() { return $this->ohjustenHinta; }
  public function getDefujenHinta()  { return $this->defujenHinta; }
  public function getAikaAlku()      { return $this->alku_aika; }
  public function getAikaLoppu()     { return $this->loppu_aika; }
  public function getDefuAlku()      { return $this->alku_defut; }
  public function getDefuLoppu()     { return $this->loppu_defut; }
  public function getDefuHajonneet() { return $this->hajonneet; }
  public function getOhjuksetLkm()   { return $this->ohjustenLkm; }
} // class
?>