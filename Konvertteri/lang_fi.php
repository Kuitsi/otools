<?php
/**
 * Konvertterin kielitiedosto
 * @author Kuitsi (http://iki.fi/kuitsi)
 * @license GPLv3 (see OTools/LICENSE.txt)
 *
 * Raporteista löytyvät avainsanat/sanonnat.
 * Kieli: suomi
 */
  $ATT      = 'Hyökkääjä';
  $DEF      = 'Puolustaja';
  $TECHS    = 'Aseet: \d+% Suojat: \d+% Panssari: \d+%';
  $TYPE     = 'Tyyppi(.+)';
  $NUMBER   = 'Yhteensä([\s\d\.]+)';
  $DESTR    = 'tuhottu';
  $END1     = 'Hyökkääjä on voittanut taistelun\!';
  $END2     = 'Taistelu päättyi tasan.+';//, molemmat laivueet palaavat kotiplaneetoilleen.
  $END3     = 'Puolustaja on voittanut taistelun\!';
  $RECYCLED           = '.*?([0-9.]+) metallia ja ([0-9.]+) kristallia leijuu avaruudessa. Olet kerännyt ([0-9.]+) metallia ja ([0-9.]+) kristallia';// numerot saa myös (\S+)
  $TOP                = 'Seuraavat laivastot kohtasivat taistelussa \(\d{1,2}.*:\d{1,2}\):?'; // version 5.2.1 tai 5.2.2 jälkeen raporteista puuttuu kaksoispiste rivin lopusta
  $INTER_ROUND        = 'Hyökkäävä laivasto tulittaa.+[\n\s]+.+\n';
  $ATT_NAME_COORDS    = '(.+)[\n ](\[\d+:\d+:\d+\])';//nimi [x:xxx:x]   joissain raporteissa nimen ja koordinaattien välissä on rivinvaihto
  $ATTDEF_DESTR       = '(.+) tuhottu';
  $STEALED            = '(Hän kaappasi[ \r\n\t]+)([\d\.]+)( metallia, )([\d\.]+)( kristallia ja )([\d\.]+)( deuteriumia\.)';
  $DEBRIS             = 'Näissä avaruuskoordinaateissa on ([\d\.]+) metallia ja ([\d\.]+) kristallia\.';

  //puolustajan rapsasta mitä defuja saatiin korjattua
  $REPAIRED_REGEXP    = "([\d\.]+ [ \wåäö]+, )*([\d\.]+ [ \wåäö]+)";
  $REPAIRED           = " yksikköä voitiin korjata";

  $dec_point = ",";
  $thousands_sep = " ";

  $hyödyllisyys = "Hyödyllisyys";

  $keruuOtsikko = "Keruuraportti";//kierrätysrapsan otsikkorivi (aika, koordit...)

  //kierrätysrapsan ja kaappausten rivitystä siistimmäksi
  $kierrätysEtsi = array(" Kohteessa", ". Olet", "\t", "Hän kaappasi ", "voi olla korjattu");
  $kierrätysKorvaa = array("\nKohteessa", ".\nOlet", "", "Hän kaappasi\n", "on korjattu");

  //poistetaan alusten ja defujen lyhenteistä välilyönnit, muuten tulee ongelmia splittauksissa
  $välinpoistoEtsi = array("Ioni T.", "Retk. alus");
  $välinpoistoKorvaa = array("Ioni_T.", "Retk._alus");

  // alusten ja defujen lyhenteet ja niitä vastaavat nimet. oltava oikeassa järjestyksessäkin
  $lyhenteet["fi"] = array(
	'R.heitin' => 'Raketinheitin',
	'K.Laser'  => 'Kevyt laser',
	'R.Laser'  => 'Raskas laser',
	'Gauss'    => 'Gaussin tykki',
	'Ioni_T.'  => 'Ionitykki',//alunperin "Ioni T."
	'Plasma'   => 'Plasmatorni',
	'P.Suoja'  => 'Pieni suoja',
	'S.Suoja'  => 'Suuri suoja',
	'P.Rahtialus'   => 'Pieni rahtialus',
	'S.Rahtialus'   => 'Suuri rahtialus',
	'K.Hävittäjä'   => 'Kevyt hävittäjä',
	'R.Hävittäjä'   => 'Raskas hävittäjä',
	'Risteilijä'    => 'Risteilijä',
	'Taistelualus'  => 'Taistelualus',
	'Retk._alus'    => 'Siirtokunta-alus',//alunperin "Retk. alus"
	'Kierr.'        => 'Kierrättäjä',
	'Vak.luotain'   => 'Vakoiluluotain',
	'Pommittaja'    => 'Pommittaja',
	'Aur.sat'       => 'Aurinkosatelliitti',
	'Tuhoaja'       => 'Tuhoaja',
	'Kuolemantähti' => 'Kuolemantähti',
	'Taistelurist.' => 'Taisteluristeilijä'
  );

  // vakoilurapsan tietoja ohjusiskua varten
  $vakoiluOtsikko = 'Resurssit ([ \w\dåäö]+) (\[\d:\d+:\d+\]) \(Pelaajat \'([\w\dåäö]+)\'\):lla (\d+-\d+ \d+:\d+:\d+)';// 0=kaikki, 1=planeetta, 2=naatit, 3=pelaaja, 4=aika
  $DEFUNIMET = array(
	'RH' => 'Raketinheitin',
	'KL' => 'Kevyt laser',
	'RL' => 'Raskas laser',
	'GT' => 'Gaussin tykki',
	'IT' => 'Ionitykki',
	'PT' => 'Plasmatorni',
	'PS' => 'Pieni suojakupu',
	'SS' => 'Suuri suojakupu',
	'TO' => 'Torjuntaohjukset',
	'PO' => 'Planeettainväliset ohjukset'
  );
  $MISSILE_DEFUT = "Puolustus";
  $MISSILE_START = "Alkutilanne";
  $MISSILE_BETWEEN = "%LKM% ohjuksen jälkeen...";
  $MISSILE_END = "Lopputilanne";
?>