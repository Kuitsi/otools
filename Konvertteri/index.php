<?php
/**
 * Konvertteri
 * @author Kuitsi (http://iki.fi/kuitsi)
 * @license GPLv3 (see OTools/LICENSE.txt)
 *
 * TODO: Bensalaskuri, jolloin riittää yksinkertaisempikin käyttäjän syöttämä tieto
 *       Bensalaskuri toimii jo, mutta ei vielä integroitu Konvertteriin: ks. bensat.php
 */

ob_start(); // tallennetaan kaikki tulosteet ensin puskuriin, jotta headerit (ja cookiet) voidaan lähettää

define("KEKSI", "OTools-Konvertteri");
if (isset($_GET["resetColors"])) {
  deleteCookie();
}
 
include "gradient.php"; // liukuvärien tuottamiseen apufunktioita sekä heksadesimaalien tarkastus
include "colorsettings.php";

// käyttäjän määrittelemiä värejä
if (isset($_POST["player_color"]))
  if (verifyHEX($_POST["player_color"]))
    $vari["nimi"] = $_POST["player_color"];
if (isset($_POST["alliance_color"]))
  if (verifyHEX($_POST["alliance_color"]))
    $vari["liittouma"] = $_POST["alliance_color"];

function koordit($koordit, $color) {
  if ($color!="") { $alku = "[color=".$color."]"; $loppu = "[/color]"; }

  if ($_POST["koordit"]=="näytä") return " ".$alku.$koordit.$loppu;
  else if ($_POST["koordit"]=="xxx") return " ".$alku."[x:xxx:x]".$loppu;
  else/* if ($_POST["koordit"]=="piilota")*/ return "";
}
function tekit($teknologiat) {
  if (isset($_POST["piilotatekit"])) return "";
  else return $teknologiat."\n";
}
function keskitys_alku() {
  if ($_POST["keskitys"]=="align") return "[align=center]";
  else if ($_POST["keskitys"]=="center") return "[center]";
  else return "";
}
function keskitys_loppu() {
  if ($_POST["keskitys"]=="align") return "[/align]";
  else if ($_POST["keskitys"]=="center") return "[/center]";
  else return "";
}
function lainaus_alku() {
 if($_POST["quote"]) return "[quote]";
 else return "";
}
function lainaus_loppu() {
 if($_POST["quote"]) return "[/quote]";
 else return "";
}
function createdBy() {
 return "Created by [url='http://board.fi.ogame.org/index.php?page=Thread&threadID=5272']Konvertteri[/url]\n&copy; Kuitsi 2009-2013";
}

function deleteCookie() {
  setcookie(KEKSI, "", 0);
  unset($cookieValues);
  unset($_COOKIE[KEKSI]);
}

echo '<?xml version="1.0" encoding="UTF-8"?>'."\n";
?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="fi" xml:lang="fi">
<head>
  <title>Konvertteri - Raportin muunnosohjelma selainpeliin OGame</title>
  <meta http-equiv="Content-Type" content="application/text+html; charset=UTF-8" />
</head>

<body style="background-image: url(background.jpg); background-repeat: no-repeat; background-position: top right;" text="#ffffff" link="#ffff00" vlink="#ffff00" bgcolor="#050E1F">

<h1>Konvertteri</h1>
<p>Konvertoi OGamen taistelurapsoja, joiden kieli on suomi tai englanti.<br />
Lue: <a href="<?php echo $_SERVER["PHP_SELF"]; ?>?lue">tavallinen taisteluraportti</a>, <a href="<?php echo $_SERVER["PHP_SELF"]; ?>?recy">pelkkä kierrätysraportti</a> 
tai <a href="<?php echo $_SERVER["PHP_SELF"]; ?>?ohjukset">ohjusiskun vakoiluraportit</a>.</p>

<hr width="75%" align="left" />

<?php
if (isset($_GET["recy"])) {
  if (isset($_GET["tulos"])) { ?>
	<form name="konvertoitu">
	<p><input type="button" value="Valitse konvertoitu raportti" onClick="this.form.tulos.focus();this.form.tulos.select();" /> ja kopioi se painamalla CTRL+C</p>
  <?php
	echo '<textarea name="tulos" rows="10" cols="100">';
	echo lainaus_alku();
	echo keskitys_alku();
	require("lang_".$_POST["kieli"].".php");
	$raportti = trim($_POST["raportti"]);

	/**
	* kierrätysrapsat
	*/
  	if (preg_match_all('/'.$RECYCLED.'/i', $raportti, $kierrätetty, PREG_SET_ORDER) > 0) {
		foreach ($kierrätetty as $kierr) {
			$rk[0] = str_replace(".", "", $kierr[1]);
			$rk[1] = str_replace(".", "", $kierr[2]);
			$kierrRK[0] = str_replace(".", "", $kierr[3]);
			$kierrRK[1] = str_replace(".", "", $kierr[4]);

			//pientä siistimistä ja numeroiden värittämistä
			$kierr[0] = str_replace($kierrätysEtsi, $kierrätysKorvaa, $kierr[0]);
			$kierr[0] = preg_replace('/.*'.$keruuOtsikko.'.*\n/i', "",$kierr[0]);
			$kierr[0] = preg_replace('/\[\d+:\d+:\d+\], /i', "", $kierr[0]);//koordinaatit pois näkyvistä
			$kierr[0] = preg_replace('/(\d+(?:\.\d+)*)/', "[color=".$vari["kaappasi"]."]$1[/color]",$kierr[0] );

			echo $kierr[0]." ([color=".$vari["kaappasi"]."]".round((($kierrRK[0]+$kierrRK[1])/($rk[0]+$rk[1])*100),1)."%[/color])\n\n";
		}
	}
	else echo "Ei kelvollisia kierrätysraportteja!\n\n";

	echo createdBy();
	echo keskitys_loppu();
	echo lainaus_loppu();
	echo "</textarea>\n";
	echo "</form>\n";
  } 
  else { ?>
<h2>Lue kierrätysraportti</h2>
<form action="<?php echo $_SERVER["PHP_SELF"]; ?>?recy&amp;tulos" method="post">
  <p style="float:left; border-right: 1px solid #000; padding-right:5px;">Raportin kieli:<br />
  <input type="radio" name="kieli" id="suomi" value="fi" checked="checked "/>
	<label for="suomi">suomi</label>
  <br />
  <input type="radio" name="kieli" id="englanti" value="en" />
	<label for="englanti">englanti</label>
  </p>
  <p style="float:left; margin-left:5px;">
  Raportin asettelu:<br />
  Lainaus (quote): <select name="quote"><option value="0">ei</option><option value="1">kyllä</option></select><br />
  Keskitys: <select name="keskitys"><option value="align">[align=center]</option><option value="center">[center]</option><option value="ei">ei mitään</option></select></p>
  <p style="clear:both;"><textarea name="raportti" rows="5" cols="100"></textarea></p>
  <p><input type="submit" value="Lue raportti" /></p>
</form>

<?php }
}// kierrätysrapsat

else if (isset($_GET["ohjukset"])) {
  if (isset($_GET["tulos"])) { ?>
	  <form name="konvertoitu">
	  <p><input type="button" value="Valitse konvertoitu raportti" onClick="this.form.tulos.focus();this.form.tulos.select();" /> ja kopioi se painamalla CTRL+C</p>
	<?php
	  echo '<textarea name="tulos" rows="10" cols="100">';
	  echo lainaus_alku();
	  echo keskitys_alku();

	  $raportti_alku = trim($_POST["raportti_alku"]);
	  $raportti_loppu = trim($_POST["raportti_loppu"]);

	  require("lang_".$_POST["kieli"].".php");
	  require("ohjusisku.php");
	  $o = new Ohjusisku($raportti_alku, $raportti_loppu, $_POST["ohjuksia"], $_POST["kieli"]);

	  $lv=isset($_POST["liukuvärit"]);//lyhennetään vähän merkintöjä
	  if ($lv) {
		$rivejä = count($o->getDefuAlku()) + 1; // liukuväritystä varten, +1="Puolustus"-otsikko
		$liuku1 = ( verifyHEX($_POST["liuku1"]) ? $_POST["liuku1"] : $vari["att1"] );
		$liuku2 = ( verifyHEX($_POST["liuku2"]) ? $_POST["liuku2"] : $vari["att2"] );
		$liuku = gradient($liuku1, $liuku2, $rivejä);
	  }

	  echo $MISSILE_START." [b]".$o->getAikaAlku()."[/b]:\n";
	  echo "\n";
	  echo ($lv ? '[color='.$liuku[0].']' : "") . "[u]".$MISSILE_DEFUT.":[/u]" . ($lv ? '[/color]' : "") . "\n";
	  $l=1;//liukuvärin laskuri
	  foreach ($o->getDefuAlku() as $lyh => $lkm) {
		echo ($lv ? '[color='.$liuku[$l].']' : "") . $DEFUNIMET[$lyh] . " " . $lkm . ($lv ? '[/color]' : "") . "\n";
		$l++;
	  }
	  echo "\n".str_replace("%LKM%", $o->getOhjuksetLkm(), $MISSILE_BETWEEN)."\n\n";
	  echo $MISSILE_END." [b]".$o->getAikaLoppu()."[/b]:\n";
	  echo "\n";
	  echo ($lv ? '[color='.$liuku[0].']' : "") . "[u]".$MISSILE_DEFUT.":[/u]" . ($lv ? '[/color]' : "") . "\n";

	  $defuLoppu = $o->getDefuLoppu();
	  $hajonneet = $o->getDefuHajonneet();
	  $l=1;//liukuvärin laskuri
	  foreach ($o->getDefuAlku() as $lyh => $lkm) {
		$tmp = ( array_key_exists($lyh, $defuLoppu) ? $defuLoppu[$lyh] : 0);
		$hajotmp = ($hajonneet[$lyh]==0 ? "" : " [color=".$vari["hajonnut"]."][b]( -".number_format($hajonneet[$lyh], 0, $dec_point, $thousands_sep)." )[/b][/color]");
		echo ($lv ? '[color='.$liuku[$l].']' : "") . $DEFUNIMET[$lyh] . " " . $tmp . $hajotmp . ($lv ? '[/color]' : "") ."\n";
		$l++;
	  }

	  echo "\n[color=".$vari["hyödyllisyys"]."]".$hyödyllisyys.":[/color]\n";
	  echo $ATT.": [color=".$vari["hyödyllisyys"]."]-".number_format($o->getOhjustenHinta(), 0, $dec_point, $thousands_sep)."[/color]\n";
	  echo $DEF.": [color=".$vari["hyödyllisyys"]."]-".number_format($o->getDefujenHinta(), 0, $dec_point, $thousands_sep)."[/color]\n";

	  echo "\n".createdBy();
	  echo keskitys_loppu();
	  echo lainaus_loppu();

	  echo "</textarea>\n";
	  echo "</form>\n";
	}//näytä ohjusiskun tulos
  else { ?>
  <script type="text/javascript" src="jscolor/jscolor.js"></script>

  <h2>Lue ohjusiskun vakoiluraportit</h2>
  <form action="<?php echo $_SERVER["PHP_SELF"]; ?>?ohjukset&amp;tulos" method="post">
  <p style="float:left; border-right: 1px solid #000; padding-right:5px;">Raportin kieli:<br />
	<input type="radio" name="kieli" id="suomi" value="fi" checked="checked "/>
	  <label for="suomi">suomi</label>
	  <br />
	<input type="radio" name="kieli" id="englanti" value="en" />
	  <label for="englanti">englanti</label>
  </p>

  <p style="float:left; margin-left:5px; border-right: 1px solid #000; padding-right:5px;">
  Raportin asettelu:<br />
  Lainaus (quote): <select name="quote"><option value="0">ei</option><option value="1">kyllä</option></select><br />
  Keskitys: <select name="keskitys"><option value="align">[align=center]</option><option value="center">[center]</option><option value="ei">ei mitään</option></select></p>

  <p style="float:left; margin-left:5px;">Liukuvärit: <input type="checkbox" name="liukuvärit" checked="checked" /> 
	<input type="text" name="liuku1" size="7" value="<?php echo $vari["att1"]; ?>" class="color {hash:true,caps:false}" /> &rarr; 
	<input type="text" name="liuku2" size="7" value="<?php echo $vari["att2"]; ?>" class="color {hash:true,caps:false}" />
	<br />
	<br />
	Ohjuksia ammuttu yhteensä: <input type="text" name="ohjuksia" size="5" />
  </p>
  
  
  <p style="clear:both;">Vakoiluraportti ennen ohjusiskua:<br /><textarea name="raportti_alku" rows="5" cols="100"></textarea></p>
  
  <p style="clear:both;">Vakoiluraportti ohjusiskun jälkeen:<br /><textarea name="raportti_loppu" rows="5" cols="100"></textarea></p>
  
  <p><input type="submit" value="Lue raportti" /></p>
  
  <?php } // lue ohjusiskun raportit
}//ohjusrapsat 

else if (isset($_GET["lue"]) || !isset($_POST["raportti"])) {

$cookieValues = unserialize(base64_decode($_COOKIE[KEKSI]));

//debug:
//echo "<pre>|"; print_r($_COOKIE[KEKSI]); echo "|</pre>";
//echo "<pre>¤"; print_r(unserialize(base64_decode($_COOKIE[KEKSI]))); echo "¤</pre>";

if ($cookieValues == "") deleteCookie();
else echo '<p><a href="?resetColors">Nollaa omat (väri)asetukset</a></p>';

?>
  <script type="text/javascript" src="jscolor/jscolor.js"></script>

  <h2>Lue taisteluraportti</h2>

  <form action="<?php echo $_SERVER["PHP_SELF"]; ?>" method="post">

  <p style="float:left; border-right: 1px solid #000; padding-right:5px;">
  Olen hyökkääjä:
  <input type="checkbox" name="olenhyökkääjä" checked="checked" /><br />
  Piilota teknologiat:
  <input type="checkbox" name="piilotatekit" checked="checked" /><br />
  Pelaajan nimen väri:
  <input type="text" name="player_color" size="7" value="<?php echo (isset($cookieValues['player_color']) ? $cookieValues['player_color'] : $vari["nimi"]); ?>" class="color {hash:true,caps:false}" /></p>

  <p style="float:left; margin-left:5px;">
  Hyökkääjän liittouma:
  <input type="text" name="att_alliance" /><br />
  Puolustajan liittouma:
  <input type="text" name="def_alliance" /><br />
  Liittouman tunnuksen väri:
  <input type="text" name="alliance_color" size="7" value="<?php echo (isset($cookieValues['alliance_color']) ? $cookieValues['alliance_color'] : $vari["liittouma"]); ?>"  class="color {hash:true,caps:false}" /></p>

  <div style="clear:both;">Kopioi koko taisteluraportti ja kaikki kierrätysraportit tähän.<br />
  <textarea name="raportti" rows="20" cols="100"></textarea></div>

  <p style="float:left; border-right: 1px solid #000; padding-right:5px;">
  Koordinaatit:<br />
  <input type="radio" name="koordit" id="koordit" value="näytä" />
	<label for="koordit">Näytä numeroina</label>
  <br />
  <input type="radio" name="koordit" id="koordit_x" value="xxx" />
	<label for="koordit_x">Muunna [x:xxx:x]</label>
  <br />
  <input type="radio" name="koordit" id="koordit_pois" value="piilota" checked="checked "/>
	<label for="koordit_pois">Piilota kokonaan</label>
  </p>

  <p style="float:left; margin-left:5px; padding-right:5px; border-right: 1px solid #000;">
  Raportin asettelu:<br />
  Lainaus (quote): <select name="quote"><option value="0">ei</option><option value="1">kyllä</option></select><br />
  Keskitys: <select name="keskitys"><option value="align">[align=center]</option><option value="center">[center]</option><option value="ei">ei mitään</option></select></p>

  <p style="float:left; margin-left:5px; padding-right:5px;">Liukuvärit:
  <input type="checkbox" name="liukuvärit" checked="checked" /> (*)<br />
  Hyökkääjä:
  <input type="text" name="att_color1" size="7" value="<?php echo (isset($cookieValues['att_color1']) ? $cookieValues['att_color1'] : $vari["att1"]); ?>" class="color {hash:true,caps:false}" /> &rarr;
  <input type="text" name="att_color2" size="7" value="<?php echo (isset($cookieValues['att_color2']) ? $cookieValues['att_color2'] : $vari["att2"]); ?>" class="color {hash:true,caps:false}" /><br />
  Puolustaja:
  <input type="text" name="def_color1" size="7" value="<?php echo (isset($cookieValues['def_color1']) ? $cookieValues['def_color1'] : $vari["def1"]); ?>" class="color {hash:true,caps:false}" /> &rarr;
  <input type="text" name="def_color2" size="7" value="<?php echo (isset($cookieValues['def_color2']) ? $cookieValues['def_color2'] : $vari["def2"]); ?>" class="color {hash:true,caps:false}" /></p>

  <p style="clear: both;">Tapahtuiko taistelu kuussa? Laske mitä se olisi vastannut kuunsaantiprosentteina: <input type="checkbox" name="kuunsaantiprosentit" /></p>

  <p><input type="submit" value="Lue raportti" /></p>

  <p>(*) vie enemmän merkkejä konvertoidussa raportissa, ei sovellu erittäin suuriin hyökkäyksiin</p>

  </form>

<?php }

else {//näytetään parsittu rapsa

// tallenna asetukset evästeeseen, mikäli ne eroavat oletuksista
unset($cookieValue); // nollataan varmuuden vuoksi
if ( verifyHEX($_POST["player_color"])   && $_POST["player_color"]   != $vari["nimi_default"] )      $cookieValue["player_color"]   = $_POST["player_color"];
if ( verifyHEX($_POST["alliance_color"]) && $_POST["alliance_color"] != $vari["liittouma_default"] ) $cookieValue['alliance_color'] = $_POST["alliance_color"];
if ( verifyHEX($_POST["att_color1"]) && $_POST["att_color1"] != $vari["att1"] ) $cookieValue["att_color1"] = $_POST["att_color1"];
if ( verifyHEX($_POST["att_color2"]) && $_POST["att_color2"] != $vari["att2"] ) $cookieValue["att_color2"] = $_POST["att_color2"];
if ( verifyHEX($_POST["def_color1"]) && $_POST["def_color1"] != $vari["def1"] ) $cookieValue["def_color1"] = $_POST["def_color1"];
if ( verifyHEX($_POST["def_color2"]) && $_POST["def_color2"] != $vari["def2"] ) $cookieValue["def_color2"] = $_POST["def_color2"];
if (isset($cookieValue)) setcookie(KEKSI, base64_encode(serialize($cookieValue)), time()+60*60*24*90); // expire in 90 days
// arrayn serialisointi vaatii avuksi base64 tai muuten data korruptoituu: http://davidwalsh.name/php-serialize-unserialize-issues
//echo "<pre>|"; print_r($cookieValue); echo "|</pre>";//debug

$raportti = $_POST["raportti"];
$olenhyökkääjä = ( isset($_POST["olenhyökkääjä"]) ? 1 : 0 );

require("raportinlukija.php");
$r = new Raportinlukija($raportti, $olenhyökkääjä);// tarvittaessa vielä käytetty kieli kolmantena ja defujen korjauskerroin (jos eri kuin 70% eli 0.7) neljäntenä parametrina

require("asetukset.php");//ladattava raportinlukijan jälkeen, tarvitaan sen ominaisuuksia
$kokoNimet = $r->getLyhenteet();//lyhenteet ja niitä vastaavat koko nimet oikealla kielellä
$defujenLyhenteet = $r->getDefuLyh();//defujen lyhenteet suoraan oikealla kielellä

require("lang_".$r->getKieli().".php");

$a1="";$a2="";$d1="";$d2="";//nää ei pitäis jäädä missään vaiheessa tyhjäks kun käytetään liukuvärejä

if ($_POST["liukuvärit"]) {
  $värejä = 24;//värejä maksimissaan 22 (=alukset+defut) + nimi + tekit. käytetään vain väridemossa
  $a1 = ( verifyHEX($_POST["att_color1"]) ? $_POST["att_color1"] : $vari["att1"] );
  $a2 = ( verifyHEX($_POST["att_color2"]) ? $_POST["att_color2"] : $vari["att2"] );
  $d1 = ( verifyHEX($_POST["def_color1"]) ? $_POST["def_color1"] : $vari["def1"] );
  $d2 = ( verifyHEX($_POST["def_color2"]) ? $_POST["def_color2"] : $vari["def2"] );

  $attLiuku = gradient($a1, $a2, $värejä);
  $defLiuku = gradient($d1, $d2, $värejä);

  $board_bg = ($_POST["quote"] ? "#171E2F" : "#1F273C");
  $quoteOrNot = ($_POST["quote"] ? "quoten pohjaväri" : "tavallinen taustaväri");
  echo "<div style=\"background-color: ".$board_bg."; color: #ffffff; padding: 5px; width: 800px; \">Varmista käytettyjen värien näkyvyys boardilla.<br />Tämä taustaväri on sama kuin board.fi.ogame.org perusteeman ".$quoteOrNot."<br />\n";
  echo "Raportissa käytetyt hyökkääjän värit: ";
  foreach ($attLiuku as $v)
	echo "<span style=\"background-color: ".$v."\">&nbsp;</span>";
  echo " ja puolustajan värit: ";
  foreach ($defLiuku as $v)
	echo "<span style=\"background-color: ".$v."\">&nbsp;</span>";
  echo "</div>\n";
}
?>
<script type="text/javascript">
function textCounter(textarea, counterID) {
	cnt = document.getElementById(counterID);
	cnt.innerHTML = textarea.value.length;
}
function poistaVärit() {
  // poistaa [color=#[a-f0-9]{6}] ja [/color]
  document.konvertoitu.tulos.value = document.konvertoitu.tulos.value.replace(/\[color=#[a-f0-9]{6}\]/g, "");
  document.konvertoitu.tulos.value = document.konvertoitu.tulos.value.replace(/\[\/color\]/g, "");
  // ja päivitä merkkilaskurin arvo
  textCounter(document.konvertoitu.tulos,'merkkilaskuri');
}
function poistaBoldaukset() {
  // poistaa [b] ja [/b] 
  document.konvertoitu.tulos.value = document.konvertoitu.tulos.value.replace(/\[[/]*b\]/g, "");
  // ja päivitä merkkilaskurin arvo
  textCounter(document.konvertoitu.tulos,'merkkilaskuri');
}
</script>

<form name="konvertoitu">
<p><input type="button" value="Valitse konvertoitu raportti" onClick="this.form.tulos.focus();this.form.tulos.select();" />
ja kopioi se painamalla CTRL+C</p>

<p>Merkkejä käytetty <span id="merkkilaskuri">0</span> / 10000 <input type="button" value="Päivitä laskuri" onClick="textCounter(document.konvertoitu.tulos,'merkkilaskuri');" /></p>

<p>Onko raportissa liikaa merkkejä? <input type="button" value="Poista värit" onClick="poistaVärit();" />
<input type="button" value="Poista lihavoinnit" onClick="poistaBoldaukset();" /></p>
<hr width="75%" align="left" />

<textarea name="tulos" cols="120" rows="25" onkeyup="textCounter(this,'merkkilaskuri');" onblur="textCounter(this,'merkkilaskuri');"><?php
echo lainaus_alku();
echo keskitys_alku();

echo str_replace(array("(", ")"), "" ,$r->getAika())."\n";
echo $erotin;

//hyökkääjät alussa
$alkuAtt=$r->getAlkuAtt();
$maaraAtt=count($alkuAtt);
for($p=0; $p<$maaraAtt; $p++){

  $kohtia = count($alkuAtt[$p])-1;//montako riviä pitäisi värittää gradientilla, koordit -= 1
  if (isset($_POST["piilotatekit"])) $kohtia -= 1;//myöskään tekkejä ei tarvitse värittää jos niitä ei edes näytetä ;)
  $v=0;//liukuvärin indeksit

  //kohtien mukaan laskettu väriskaalaus
  $attLiuku = gradient($a1, $a2, $kohtia);

  $d=0;
  if ($_POST["liukuvärit"]) echo "[color=".$attLiuku[$v++]."]";
  elseif(isset($_POST["olenhyökkääjä"])) echo "[color=".$vari["oma"]."]"; else echo "[color=".$vari["vihu"]."]";
  $liittouma = ($_POST["att_alliance"] != "") ? " [color=".$vari["liittouma"]."]( ".$_POST["att_alliance"]." )[/color]" : "";
  while ($value = current($alkuAtt[$p])) {
	$lyh = key($alkuAtt[$p]);
	if ($lyh == 'nimi') echo $hyökkääjä.": [color=".$vari["nimi"]."]".$value."[/color]".$liittouma.($_POST["liukuvärit"] ? "[/color]" : "");
	elseif ($lyh == 'koordinaatit') echo koordit($value,$attLiuku[0]);
	elseif ($lyh == 'tekit') {
		if (!isset($_POST["piilotatekit"]) && $_POST["liukuvärit"])
			$value = "[color=".$attLiuku[$v++]."]".$value."[/color]";
		echo "\n".tekit($value);
	}
	elseif ($lyh == 'tuhottu') { if ($_POST["liukuvärit"]) $tuhottu = "[color=".$attLiuku[$v++]."]".$tuhottu."[/color]"; echo $tuhottu."\n\n"; break; }
	else {
	  if ($d==0) {
		foreach ($defujenLyhenteet as $dl) {
		  if ($lyh==$dl) { $d++; echo "\n"; break; }
		}
	  }
	  $value = number_format($value, 0, $dec_point, $thousands_sep);
	  if ($_POST["liukuvärit"]) $value = "[color=".$attLiuku[$v++]."]".(isset($kokoNimet[$lyh]) ? $kokoNimet[$lyh] : $lyh) .": ".$value."[/color]";
	  else $value = (isset($kokoNimet[$lyh]) ? $kokoNimet[$lyh] : $lyh) .": ".$value;
	  echo "\n".$value;
	}
    next($alkuAtt[$p]);
  }
  echo ($_POST["liukuvärit"] ? "\n" : "[/color]\n");
  
  echo $erotin;
}

//puolustajat alussa
$alkuDef=$r->getAlkuDef();
$maaraDef=count($alkuDef);
for($p=0; $p<$maaraDef; $p++){

  $kohtia = count($alkuDef[$p])-1;//montako riviä pitäisi värittää gradientilla, koordit -= 1
  if (isset($_POST["piilotatekit"])) $kohtia -= 1;//myöskään tekkejä ei tarvitse värittää jos niitä ei edes näytetä ;)
  $v=0;//liukuvärin indeksit

  //kohtien mukaan laskettu väriskaalaus
  $defLiuku = gradient($d1, $d2, $kohtia);

  $d=0;
  if ($_POST["liukuvärit"]) echo "[color=".$defLiuku[$v++]."]";
  elseif(isset($_POST["olenhyökkääjä"])) echo "[color=".$vari["vihu"]."]"; else echo "[color=".$vari["oma"]."]";
  $liittouma = ($_POST["def_alliance"] != "") ? " [color=".$vari["liittouma"]."]( ".$_POST["def_alliance"]." )[/color]" : "";
  while ($value = current($alkuDef[$p])) {
	$lyh = key($alkuDef[$p]);
	if ($lyh == 'nimi') echo $puolustaja.": [color=".$vari["nimi"]."]".$value."[/color]".$liittouma.($_POST["liukuvärit"] ? "[/color]" : "");
	elseif ($lyh == 'koordinaatit') echo koordit($value,$defLiuku[0]);
	elseif ($lyh == 'tekit') {
		if (!isset($_POST["piilotatekit"]) && $_POST["liukuvärit"])
			$value = "[color=".$defLiuku[$v++]."]".$value."[/color]";
		echo "\n".tekit($value);
	}
	elseif ($lyh == 'tuhottu') { if ($_POST["liukuvärit"]) $tuhottu = "[color=".$defLiuku[$v++]."]".$tuhottu."[/color]"; echo "\n"; if(count($alkuDef[$p])==2){echo "\n";} echo $tuhottu; break; }
	else {
	  if ($d==0) {
		foreach ($defujenLyhenteet as $dl) {
		  if ($lyh==$dl) { $d++; echo "\n"; break; }
		}
	  }
	  $value = number_format($value, 0, $dec_point, $thousands_sep);
	  if ($_POST["liukuvärit"]) $value = "[color=".$defLiuku[$v++]."]".(isset($kokoNimet[$lyh]) ? $kokoNimet[$lyh] : $lyh) .": ".$value."[/color]";
	  else $value = (isset($kokoNimet[$lyh]) ? $kokoNimet[$lyh] : $lyh) .": ".$value;
	  echo "\n".$value;
	}
    next($alkuDef[$p]);
  }
  echo ($_POST["liukuvärit"] ? "\n" : "[/color]\n");

  echo $erotin;
}

echo $kierroksiaTeksti."\n";
echo $erotin;

//hyökkääjät lopussa
$loppuAtt=$r->getLoppuAtt();
for($p=0; $p<$maaraAtt; $p++){

  $kohtia = count($loppuAtt[$p]);//montako riviä pitäisi värittää gradientilla
  if (isset($loppuAtt[$p]["koordinaatit"])/* && $_POST["koordit"]=="piilota"*/) $kohtia--;
  $v=0;//liukuvärin indeksit

  //kohtien mukaan laskettu väriskaalaus
  $attLiuku = gradient($a1, $a2, $kohtia);

  $d=0;
  if ($_POST["liukuvärit"]) echo "[color=".$attLiuku[$v++]."]";
  elseif(isset($_POST["olenhyökkääjä"])) echo "[color=".$vari["oma"]."]"; else echo "[color=".$vari["vihu"]."]";
  while ($value = current($loppuAtt[$p])) {
	$lyh = key($loppuAtt[$p]);
	if ($lyh == 'nimi') echo $hyökkääjä.": [color=".$vari["nimi"]."]".$value."[/color]".($_POST["liukuvärit"] ? "[/color]" : "");
	elseif ($lyh == 'koordinaatit') echo /*koordit($value,$attLiuku[0]).*/"\n";
	elseif ($lyh == 'tuhottu') { if ($_POST["liukuvärit"]) $tuhottu = "[color=".$attLiuku[$v++]."]".$tuhottu."[/color]"; echo "\n".$tuhottu; break; }
	else {
	  if ($d==0) {
		foreach ($defujenLyhenteet as $dl) {
		  if ($lyh==$dl) { $d++; echo "\n"; break; }
		}
	  }
	  $hajonneet = ( (isset($alkuAtt[$p][$lyh]) && $alkuAtt[$p][$lyh]!=$value) ? " [color=".$vari["hajonnut"]."][b]( -".number_format(($alkuAtt[$p][$lyh]-$value), 0, $dec_point, $thousands_sep)." )[/b][/color]" : "");
	  $value = number_format($value, 0, $dec_point, $thousands_sep);
	  if ($_POST["liukuvärit"]) $value = "[color=".$attLiuku[$v++]."]".(isset($kokoNimet[$lyh]) ? $kokoNimet[$lyh] : $lyh) .": ".$value."[/color]";
	  else $value = (isset($kokoNimet[$lyh]) ? $kokoNimet[$lyh] : $lyh) .": ".$value;
	  echo "\n".$value.$hajonneet;
	}
    next($loppuAtt[$p]);
  }
  echo ($_POST["liukuvärit"] ? "\n" : "[/color]\n");

  echo $erotin;
}

//puolustajat lopussa
$loppuDef=$r->getLoppuDef();
for($p=0; $p<$maaraDef; $p++){

  $kohtia = count($loppuDef[$p]);//montako riviä pitäisi värittää gradientilla
  if (isset($loppuDef[$p]["koordinaatit"])/* && $_POST["koordit"]=="piilota"*/) $kohtia--;
  $v=0;//liukuvärin indeksit

  //kohtien mukaan laskettu väriskaalaus
  $defLiuku = gradient($d1, $d2, $kohtia);

  $d=0;
  if ($_POST["liukuvärit"]) echo "[color=".$defLiuku[round($värejä*$v++*(1/($kohtia)))]."]";
  elseif(isset($_POST["olenhyökkääjä"])) echo "[color=".$vari["vihu"]."]"; else echo "[color=".$vari["oma"]."]";
  while ($value = current($loppuDef[$p])) {
	$lyh = key($loppuDef[$p]);
	if ($lyh == 'nimi') echo $puolustaja.": [color=".$vari["nimi"]."]".$value."[/color]".($_POST["liukuvärit"] ? "[/color]" : "");
	elseif ($lyh == 'koordinaatit') echo /*koordit($value,$defLiuku[0]).*/"\n";
	elseif ($lyh == 'tuhottu') { if ($_POST["liukuvärit"]) $tuhottu = "[color=".$defLiuku[$v++]."]".$tuhottu."[/color]"; echo "\n"; if(count($loppuDef[$p])==2){echo "\n";} echo $tuhottu; break; }
	else {
	  if ($d==0) {
		foreach ($defujenLyhenteet as $dl) {
		  if ($lyh==$dl) { $d++; echo "\n"; break; }
		}
	  }
	  $hajonneet = ( (isset($alkuDef[$p][$lyh]) && $alkuDef[$p][$lyh]!=$value) ? " [color=".$vari["hajonnut"]."][b]( -".number_format(($alkuDef[$p][$lyh]-$value), 0, $dec_point, $thousands_sep)." )[/b][/color]" : "");
	  $value = number_format($value, 0, $dec_point, $thousands_sep);
	  if ($_POST["liukuvärit"]) $value = "[color=".$defLiuku[$v++]."]".(isset($kokoNimet[$lyh]) ? $kokoNimet[$lyh] : $lyh) .": ".$value."[/color]";
	  else $value = (isset($kokoNimet[$lyh]) ? $kokoNimet[$lyh] : $lyh) .": ".$value;
	  echo "\n".$value.$hajonneet;
	}
    next($loppuDef[$p]);
  }
  echo ($_POST["liukuvärit"] ? "\n" : "[/color]\n");

  echo $erotin;
}

$loppurapsa = $r->getLoppurapsa();

//hän kaappasi...
$pattern = '/'.$STEALED.'/i';//'/('.$kaappasi.')([\d\.]+)([^\d]+)([\d\.]+)([^\d]+)([\d\.]+)([^\d]*)/';
$replacement = '$1[color='.$vari["kaappasi"].']$2[/color]$3[color='.$vari["kaappasi"].']$4[/color]$5[color='.$vari["kaappasi"].']$6[/color]$7';
$loppurapsa = preg_replace($pattern, $replacement, $loppurapsa);

//menetti yhteensä
$attdef_pattern = '/('.$hyökkääjä.'|'.$puolustaja.')( '.$menetti.'[^\d]+)([\d\.]+)(.+)/i';
function menetetyt($matches){
  global $vari, $hyökkääjä, $puolustaja;
  // as usual: $matches[0] is the complete match,  $matches[1] is the match for the
  // first subpattern enclosed in '(...)' and so on
  if (isset($_POST["olenhyökkääjä"])) {
	if (stripos($matches[1], $hyökkääjä)!== false)
	  $color = $vari["oma"];
	else
	  $color = $vari["vihu"];
  }
  else {
	if (stripos($matches[1], $puolustaja)!== false)
	  $color = $vari["oma"];
	else
	  $color = $vari["vihu"];
  }
  $tulos = '[color='.$color.']'.$matches[1].'[/color]'.$matches[2];
  $tulos .= '[color='.($matches[3] == "0" ? $vari["ehjänä"] : $vari["tappiolla"]).']'.$matches[3].'[/color]'.$matches[4];
  return $tulos;
}
$loppurapsa = preg_replace_callback($attdef_pattern, "menetetyt", $loppurapsa);

//muodostunut romukenttä
$rk_pattern = '/('.$debrisformed.')( [^\d]+)([\d\.]+)([^\d]+)([\d\.]+)([^\d]+)/i';
$rk_replacement = '$1$2[color='.$vari["RK"].']$3[/color]$4[color='.$vari["RK"].']$5[/color]$6';
$loppurapsa = preg_replace($rk_pattern, $rk_replacement, $loppurapsa);

//kuun muodostuminen
$rk_pattern = '/(.*)('.$kuu.')([^\d]+)(\d+)([^\d]+)/i';
$rk_replacement = '$1$2$3[color='.$vari["kuu"].']$4[/color]$5';
$loppurapsa = preg_replace($rk_pattern, $rk_replacement, $loppurapsa);

//kierrätystä ja muuta siistimistä
$loppurapsa = str_replace($kierrätysEtsi, $kierrätysKorvaa, $loppurapsa);
$loppurapsa = preg_replace('/.*'.$keruuOtsikko.'.*\n/i', "", $loppurapsa);
$loppurapsa = preg_replace('/ \[\d+:\d+:\d+\],/i', "", $loppurapsa);//koordinaatit pois kierrätysrapsasta

echo $loppurapsa."\n";

/**
 * laskennallinen kuunsaantiprosentti
 */
if (isset($_POST["kuunsaantiprosentit"])) {
	$debris = $r->getRK();
	$rk_prossat = floor(($debris[0]+$debris[1])/100000);
	if ($rk_prossat > 20) $rk_prossat = 20;
	if ($rk_prossat >= 1) echo "\nTaistelu tapahtui kuussa. Se olisi vastannut ".$rk_prossat."% kuunsaantimahdollisuutta.\n";
}

echo $erotin;

$kierrRK = $r->getKierrätetty();
$rk = $r->getRK();
if (!empty($kierrRK)) {
  echo ($olenhyökkääjä ? $hyökkääjä : $puolustaja) . " ".$recycledTotal." [color=".$vari["kaappasi"]."]".number_format($kierrRK[0], 0, $dec_point, $thousands_sep)."[/color] ".$recycledMetal." [color=".$vari["kaappasi"]."]".number_format($kierrRK[1], 0, $dec_point, $thousands_sep)."[/color] ".$recycledCrystal."\n([color=".$vari["kaappasi"]."]".round((($kierrRK[0]+$kierrRK[1])/($rk[0]+$rk[1])*100),1)."%[/color]".$syntyneestäRKsta.")\n\n";
}

// hyödyllisyyden tekstejä
$ilmanRK = (empty($kierrRK) ? $noDebris : "");
$mukanaRK = (empty($kierrRK) ? $withDebris : "");
// ja varsinainen lukuarvo hyödyllisyydelle rk:n kanssa
$attRK = (empty($kierrRK) ? ($r->getAttTuotot()+$rk[0]+$rk[1]) : "");
$defRK = (empty($kierrRK) ? ($r->getDefTuotot()+$rk[0]+$rk[1]) : "");

// hyödyllisyys ilman rk:ta. pitää selvittää ennen kulutusta niin saadaan sekin laskettua helpommin mukaan
$att_eiRK = $r->getAttTuotot();
$def_eiRK = $r->getDefTuotot();

//siistitään numerot hyödyllisyyttä varten
if ($attRK!="") $attRK = number_format($attRK, 0, $dec_point, $thousands_sep);
if ($defRK!="") $defRK = number_format($defRK, 0, $dec_point, $thousands_sep);
$att_eiRK = number_format($att_eiRK, 0, $dec_point, $thousands_sep);
$def_eiRK = number_format($def_eiRK, 0, $dec_point, $thousands_sep);

echo "[color=".$vari["hyödyllisyys"]."]".$hyödyllisyys.":[/color]\n";
echo $hyökkääjä .       $ilmanRK.": ".'[color='.$vari["hyöd_RK"].']'.$att_eiRK.'[/color]'.$mukanaRK.'[color='.$vari["hyöd_RK"].']'.$attRK."[/color]\n";
echo $puolustaja .      $ilmanRK.": ".'[color='.$vari["hyöd_RK"].']'.$def_eiRK.'[/color]'.$mukanaRK.'[color='.$vari["hyöd_RK"].']'.$defRK."[/color]\n";

/*
// TODO:
echo "[color=".$vari["hyödyllisyys"]."]Puolustajan hyödyllisyys puolustuksen korjautumisen jälkeen:[/color]\n";
if (!$olenhyökkääjä) {
  echo "puolustajan rapsa: tähän tarkka defujen korjaantuminen\n";
}
else {
  echo "Hyökkääjän rapsa, joten arvioidaan defujen korjaantuminen insinöörillä ja ilman insinööriä\n";
  //echo $insilmanRK.": ".'[color='.$vari["hyöd_RK"].']'.$def_eiRK.'[/color]'.$mukanaRK.'[color='.$vari["hyöd_RK"].']'.$defRK."[/color]\n";
}

$r->getDefKorjatut() $r->getDefKorjatutIns()
*/

echo "\n".createdBy();

echo keskitys_loppu();
echo lainaus_loppu();

echo "</textarea>\n";
?>
<script type="text/javascript">
  //siistitään turhat värimääritykset pois
  document.konvertoitu.tulos.value = document.konvertoitu.tulos.value.replace(/\[color=#[a-f0-9]{6}\]\[\/color\]/g, "");
  //päivittää käytettyjen merkkien määrän heti sivun latauksen yhteydessä
  textCounter(document.konvertoitu.tulos,'merkkilaskuri');
</script>
<?php
echo "</form>\n";
//echo "<pre>"; print_r($alkuDef); echo "</pre>";
//echo "<pre>"; print_r($loppuDef); echo "</pre>";
//preg_match_all('/'.$STEALED.'/i', $raportti, $kaapattu, PREG_SET_ORDER);
//echo "<pre>"; print_r($kaapattu); echo "</pre>";

//echo "<pre>"; print_r($r); echo "</pre>";


/*
echo (isset($alkuDef["nimi"]) && $alkuDef["nimi"]!="" ? $alkuDef["nimi"] : "ei nimeä tai se on tyhjä");*/

}//else, näytetään parsittu rapsa
?>
  <hr width="75%" align="left" />
  <p>Palautetta vastaanotetaan <a href="http://board.fi.ogame.org/index.php?page=Thread&amp;threadID=5272" target="_blank">OGame.fi:n boardilla</a></p>
  <p>Copyright &copy; 2009-2013 <a href="http://iki.fi/kuitsi/ogame/">Kuitsi</a>. Released under <a rel="license" href="http://www.gnu.org/licenses/gpl-3.0.txt">GPLv3 license</a>.
    <br />Taustakuva: Copyright &copy; Gameforge Productions GmbH<br />Värityökalu: <a href="http://jscolor.com/">jscolor</a> Copyright &copy; Jan Odvárko</p>

</body>
</html>
<?php
ob_end_flush(); // tulostetaan kaikki puskuriin kertynyt tieto
?>