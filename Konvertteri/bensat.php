<?php
/**
 * Polttoainelaskuri selainpeliin OGame
 * @author Kuitsi (http://iki.fi/kuitsi)
 * @license GPLv3 (see OTools/LICENSE.txt)
 *
 * Lisätietoja käytetyistä kaavoista: http://ogame.wikia.com/wiki/Talk:Fuel_Consumption
 */

define("HTML", true);// näytetäänkö tulos html-sivuna (true)
// TODO: false mahdollistaa tietojen välityksen suoraan toiselle ohjelmalle, esim Konvertterille

ob_start();//tulosteet puskuriin. ne näytetään vain, jos tarvitaan html-sivu tulosteena

/**
 * Muuntaa $sekunnit (INT) muotoon hh:mm:ss
 * Jos $päivät on true, palauttaa taulukon, jossa ensimmäisessä
 * solussa päivät (INT), toisessa solussa aika muodossa hh:mm:ss
 */
function secondsToTime($sekunnit, $päivät = false) {

  if ( !preg_match("/^\d+$/", $sekunnit) )
	return "Virheellinen aika!";

  $d = floor($sekunnit / 86400);          // päivät   86400s = 3600*24
  $h = floor(($sekunnit % 86400) / 3600); // tunnit   3600s = 60*60
  $m = floor(($sekunnit % 3600) / 60);    // minuutit 60s
  $s = $sekunnit % 60;                    // sekunnit

  // etunollat numeroihin
  if ($h < 10) $h = "0$h";
  if ($m < 10) $m = "0$m";
  if ($s < 10) $s = "0$s";

  if ($päivät) {
	$tulos[0] = $d;
	$tulos[1] = "$h:$m:$s";
  }
  else $tulos = "$h:$m:$s";

  return $tulos;
}

function etäisyys($lähtökoordit, $kohdekoordit) {
  if ( !preg_match("/^\d{1}:\d{1,3}:\d{1,2}$/", $lähtökoordit) || !preg_match("/^\d{1}:\d{1,3}:\d{1,2}$/", $kohdekoordit) )
	return "<b>Virheelliset koordinaatit!</b>";
	
  list($galaksi1,$systeemi1,$planeetta1)=split(":",$lähtökoordit);
  list($galaksi2,$systeemi2,$planeetta2)=split(":",$kohdekoordit);

  // lasketaan etäisyys
  if ($galaksi1 != $galaksi2)
    $etäisyys = 20000 * abs($galaksi1 - $galaksi2);
  else if ($systeemi1 != $systeemi2)
	$etäisyys = 95 * abs($systeemi1 - $systeemi2) + 2700;
  else if ($planeetta1 != $planeetta2)
	$etäisyys = 5 * abs($planeetta1 - $planeetta2) + 1000;
  else $etäisyys = 5;
  
  return $etäisyys;
}

/**
 * Palauttaa lentoajan sekunneissa (INT) kun tiedetään käytettävän aluksen todellinen nopeus ($nopeus),
 * lentonopeus prosenteissa ($prosentit) sekä lentomatka ($etäisyys).
 */
function lentoaika($nopeus, $prosentit, $etäisyys) {

  if ( !preg_match("/^\d+$/", $nopeus) )
	return "Virheellinen nopeus!";

  // lentoaika perustuen lentomatkaan
  $sekunnit = 10 + (35000 / $prosentit * sqrt( $etäisyys * 1000 / $nopeus));
  return round($sekunnit);
}

function getShipDetails(){
  // moottori: 1=poltto, 2=impulssi, 3=hyper
  // MUISTA KÄSITELLÄ NOPEUKSIEN POIKKEUDET (PR, PM) ERIKSEEN, RIVI ~175
  return Array(
  //  tyyppi => moottori, perusnopeus, perusnopeus2, kulutus, kulutus2, rahtitila
  //            0,        1,           2,            3,       4,        5
  "PR" => Array(1,        5000,        10000,        10,      20,       5000),   //PR
  "SR" => Array(1,        7500,        0,            50,      0,        25000),  //SR
  "KH" => Array(1,        12500,       0,            20,      0,        50),     //KH
  "RH" => Array(2,        10000,       0,            75,      0,        100),    //RH
  "RS" => Array(2,        15000,       0,            300,     0,        800),    //RS
  "TA" => Array(3,        10000,       0,            500,     0,        1500),   //TA
  "SA" => Array(2,        2500,        0,            1000,    0,        7500),   //SA
  "KR" => Array(1,        2000,        0,            300,     0,        20000),  //KR
  "VL" => Array(1,        100000000,   0,            1,       0,        0),      //VL
  "PM" => Array(2,        4000,        5000,         1000,    0,        500),    //PM
  "TH" => Array(3,        5000,        0,            1000,    0,        2000),   //TH
  "KT" => Array(3,        100,         0,            1,       0,        1000000),//KT
  "TR" => Array(3,        10000,       0,            250,     0,        750)     //TR
);
}

echo '<?xml version="1.0" encoding="UTF-8"?>'."\n"; ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="fi" lang="fi">
<head>
  <title>Bensankulutus</title>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
</head>

<body>
<div style="width: 700px;">

<table>
	<form method="post" action="<?php echo $_SERVER["PHP_SELF"]; ?>">
		<input type="hidden" name="mode" value="lentoajat" />
	<tr>
		<td valign="top">Alusten määrät:</td>
 		<td>
		  <table cellpadding="0">
			<tr>
			  <td><input type="text" name="alukset[PR]" value="<?php echo $_POST["alukset"]["PR"]; ?>" size="3" /> <abbr title="Pieni rahtialus">PR</abbr>,</td>
			  <td><input type="text" name="alukset[SR]" value="<?php echo $_POST["alukset"]["SR"]; ?>" size="3" /> <abbr title="Suuri rahtialus">SR</abbr>,</td>
			  <td><input type="text" name="alukset[KH]" value="<?php echo $_POST["alukset"]["KH"]; ?>" size="3" /> <abbr title="Kevyt Hävittäjä">KH</abbr>,</td>
			  <td><input type="text" name="alukset[RH]" value="<?php echo $_POST["alukset"]["RH"]; ?>" size="3" /> <abbr title="Raskas Hävittäjä">RH</abbr>,</td>
			  <td><input type="text" name="alukset[RS]" value="<?php echo $_POST["alukset"]["RS"]; ?>" size="3" /> <abbr title="Risteilijä">RS</abbr>,</td>
			  <td><input type="text" name="alukset[TA]" value="<?php echo $_POST["alukset"]["TA"]; ?>" size="3" /> <abbr title="Taistelualus">TA</abbr>,</td>
			  <td><input type="text" name="alukset[SA]" value="<?php echo $_POST["alukset"]["SA"]; ?>" size="3" /> <abbr title="Siirtokunta-alus">SA</abbr></td>
			</tr>
			<tr>
			  <td><input type="text" name="alukset[KR]" value="<?php echo $_POST["alukset"]["KR"]; ?>" size="3" /> <abbr title="Kierrättäjä">KR</abbr>,</td>
			  <td><input type="text" name="alukset[VL]" value="<?php echo $_POST["alukset"]["VL"]; ?>" size="3" /> <abbr title="Vakoiluluotain">VL</abbr>,</td>
			  <td><input type="text" name="alukset[PM]" value="<?php echo $_POST["alukset"]["PM"]; ?>" size="3" /> <abbr title="Pommittaja">PM</abbr>,</td>
			  <td><input type="text" name="alukset[TH]" value="<?php echo $_POST["alukset"]["TH"]; ?>" size="3" /> <abbr title="Tuhoaja">TH</abbr>,</td>
			  <td><input type="text" name="alukset[KT]" value="<?php echo $_POST["alukset"]["KT"]; ?>" size="3" /> <abbr title="Kuolemantähti">KT</abbr>,</td>
			  <td><input type="text" name="alukset[TR]" value="<?php echo $_POST["alukset"]["TR"]; ?>" size="3" /> <abbr title="Taisteluristeilijä">TR</abbr></td>
			</tr>
		  </table>
		</td>
	</tr>
	<tr>
		<td>Lähtökoordinaatit:</td>
		<td><input name="kääntymiskoordinaatit" type="text" maxlength="8" size="8" value="<?php if (isset($_POST["kääntymiskoordinaatit"])) echo $_POST["kääntymiskoordinaatit"]; else echo "g:sss:pp"; ?>" onClick="if(this.value == 'g:sss:pp') this.value = '';" onBlur="if(this.value == '') this.value= 'g:sss:pp';"></td>
	</tr>
	<tr>
		<td>Tappelukoordinaatit:</td>
		<td><input name="paluukoordinaatit" type="text" maxlength="8" size="8" value="<?php if (isset($_POST["paluukoordinaatit"])) echo $_POST["paluukoordinaatit"]; else echo "g:sss:pp"; ?>" onClick="if(this.value == 'g:sss:pp') this.value = '';" onBlur="if(this.value == '') this.value= 'g:sss:pp';"></td>
	</tr><?php	
	// käytetään ensisijaisesti syötettyjä tekkeja, varalla tietokannasta haetut ja viimeisenä varalla 0
	$poltto = ( preg_match("/^\d+$/", $_POST["poltto"]) ? $_POST["poltto"] : /*( $ajotekit != null ? $ajotekit[0] :*/ 0 /*)*/ );
	$impulssi = ( preg_match("/^\d+$/", $_POST["impulssi"]) ? $_POST["impulssi"] : /*( $ajotekit != null ? $ajotekit[1] :*/ 0 /*)*/ );
	$hyper = ( preg_match("/^\d+$/", $_POST["hyper"]) ? $_POST["hyper"] : /*( $ajotekit != null ? $ajotekit[2] :*/ 0 /*)*/ );
	?>
	<tr>
		<td>Ajoteknologiat:</td>
 		<td>Poltto: <input name="poltto" type="text" maxlength="2" size="2" value="<?php if (isset($poltto)) echo $poltto; ?>"> Impulssi: <input name="impulssi" type="text" maxlength="2" size="2" value="<?php if (isset($impulssi)) echo $impulssi; ?>"> Hyper: <input name="hyper" type="text" maxlength="2" size="2" value="<?php if (isset($hyper)) echo $hyper; ?>"></td>
	</tr>
	<tr>
		<td colspan="2"><input type="submit" value="Laske" /></td>
	</tr>
	</form>
</table>
<?php
if ($_POST["mode"]=="lentoajat") {

  if ( !preg_match("/^\d{1}:\d{1,3}:\d{1,2}$/", $_POST["kääntymiskoordinaatit"]) || !preg_match("/^\d{1}:\d{1,3}:\d{1,2}$/", $_POST["paluukoordinaatit"]) )
  echo "<p>Virheelliset koordinaatit!</p>\n";

  $moottoritekit = array($poltto, $impulssi, $hyper);
  $data2 = getShipDetails();
  $spds = array();//laivueen eri alusten nopeudet kyseisillä ajotekeillä. temppi

  // laivueessa mukana olleet alukset, ei huomida alle 1 aluksen määriä ;)
  $laivue = array();
  foreach ($_POST["alukset"] as $nimi => $lkm)
    if ($lkm>0) $laivue[$nimi] = $lkm;

// selvitetään laivueen hitain nopeus
echo "<p>Mukana olleet alukset ja niiden erilliset nopeudet:\n";
foreach ($laivue as $laiva => $lkm) {

  // data2-taulukon sisältö:
  // lyhenne => moottori, perusnopeus, perusnopeus2, kulutus, kulutus2, rahtitila

  // perusnopeus * (1 + moottorin_taso * moottorin_nopeuskerroin)
  $spd_tmp = $data2[$laiva][1] * (1 + $moottoritekit[$data2[$laiva][0]-1] * $data2[$laiva][0]/10);

  //poikkeukset nopeuksiin
  if ($data2[$laiva][2] != 0) {
    if (key($data2[$laiva])=="PR" && $moottoritekit[1]>4) $spd_tmp = $data2[$laiva][1] * (1 + $moottoritekit[1] * 2 / 10);
    if (key($data2[$laiva])=="PM" && $moottoritekit[2]>7) $spd_tmp = $data2[$laiva][1] * (1 + $moottoritekit[2] * 3 / 10);
  }

  $spds[$laiva] = $spd_tmp;
  echo "\n<br />$laiva: $spd_tmp\n";
}
echo "</p>\n";

$maxspeed = min($spds);//laivueen maksiminopeus on sen hitaimman aluksen nopeus
echo "<p>Laivueen nopeus: $maxspeed</p>\n";

$prosentit = array(100, 90, 80, 70, 60, 50, 40, 30, 20, 10);// lentonopeudet prosenteissa
$lentomatka = etäisyys($_POST["kääntymiskoordinaatit"], $_POST["paluukoordinaatit"]);
$speedfactor = 1;//universumin nopeus: tuplanopeuksisille universumeille 2, 5x=5 jne
$kokonaiskulutus = 0;

//print_r($spds);//jokaisen alustyypin nopeus (nimi => nopeus)

echo "<p>lentomatka: $lentomatka</p>\n";
echo "<table cellpadding=\"5\">\n";
echo "\t<tr><td>Nopeus</td><td>Lentoaika</td><td>Kulutus</td></tr>\n";
foreach ($prosentit as $p) {
  $duration = lentoaika($maxspeed, $p, $lentomatka) / $speedfactor;
//	echo "<p>lentoaika (s): $duration</p>\n";

  // jokaiselle mukana olleelle alustyypille omat laskut suhteelliselle nopeudelle ja kulutukselle
  foreach ($spds as $laiva => $shipSpeed) {
    $speed_k = 35000 / ($duration - 10) * sqrt($lentomatka * 10 / $shipSpeed);
//	echo "<p>speed_k: $speed_k</p>\n";

    $peruskulutus_k = $data2[$laiva][3];
    //poikkeukset kulutuksiin:
    if ($data2[$laiva][4] != 0) {
      if (key($data2[$laiva])=="PR" && $moottoritekit[1]>4) $peruskulutus_k = $data2[$laiva][4];
    }
//    if ($laiva=="PR" && $impulssi>=5) $peruskulutus_k = 20;

    $kulutus_k = $peruskulutus_k * $laivue[$laiva] * $lentomatka / 35000 * pow(($speed_k / 10 + 1), 2);
    $kokonaiskulutus += $kulutus_k;
//	echo "<p>alusten $laiva kulutus nopeudella $p: $kulutus_k</p>\n";
  }

// $sekunnit = 10 + (35000 / $prosentit * sqrt( $etäisyys * 1000 / $nopeus));
//  $nopeus = 35000 / ($lentoaika * $speedfactor - 10) * sqrt($lentomatka * 10 / current($nopeudet));

  echo "\t<tr><td>$p%</td><td>".secondsToTime($duration)."</td><td>".ceil($kokonaiskulutus)."</td></tr>\n";

  // nollataan kulutus seuraavan nopeuden laskuja varten
  $kokonaiskulutus = 0;
}
echo "</table>\n";

//spd = nopeus
//enf = etäisyys

}// lentoaikojen laskeminen

?>
</div>
</body>
</html>
<?php if (HTML) ob_end_flush(); ?>