<?php
/**
 * Konvertterin kielitiedosto
 * @author Kuitsi (http://iki.fi/kuitsi)
 * @license GPLv3 (see OTools/LICENSE.txt)
 *
 * Raporteista löytyvät avainsanat/sanonnat.
 * Kieli: englanti
 */
  $ATT      = 'Attacker';
  $DEF      = 'Defender';
  $TECHS    = 'Weapons: \d+% Shields: \d+% Armour: \d+%';
  $TYPE     = 'Type(.+)';
  $NUMBER   = 'Total([\s\d\.]+)';
  $DESTR    = 'Destroyed';
  $END1     = '.*Attacker has won the battle\!.*';
  $END2     = 'The combat ends in a draw.+';// - both remaining fleets return back to their home-planets.
  $END3     = '.*Defender has won the battle\!.*';
  $RECYCLED           = '.*?([\d\.]+) metal and ([\d\.]+) crystal are floating in space\. You have harvested ([\d\.]+) metal and ([\d\.]+) crystal\.';
  $TOP                = 'On \(\d{1,2}.*:\d{1,2}\) the following fleets met in battle:';
  $INTER_ROUND        = 'The attacking fleet fires .+[\n\s]+.+\n';
  $ATT_NAME_COORDS    = '(.+) (\[\d+:\d+:\d+\])';// nimi [x:xxx:x]
  $ATTDEF_DESTR       = '(.+) destroyed';
  $STEALED            = '(He captured[ \r\n\t]+)([\d\.]+)( metal, )([\d\.]+)( crystal and )([\d\.]+)( deuterium\.)';
  $DEBRIS             = 'At these space coordinates now float ([\d\.]+) metal and ([\d\.]+) crystal\.';

  //puolustajan rapsasta mitä defuja saatiin korjattua
  $REPAIRED_REGEXP    = "([\d\.]+ [ \w]+, )*([\d\.]+ [ \w]+)";
  $REPAIRED           = " could be repaired";

  $dec_point = ".";
  $thousands_sep = ".";//,

  $hyödyllisyys = "Profitability";

  $keruuOtsikko = "Harvesting report";//kierrätysrapsan otsikkorivi (aika, koordit...)

  //kierrätysrapsan ja kaappausten rivitystä siistimmäksi
  $kierrätysEtsi = array(" At the target", " You have harvested", "\t", "battle! ", "He captured ");
  $kierrätysKorvaa = array("\nAt the target", "\nYou have harvested", "", "battle!\n", "\nHe captured\n");

  //poistetaan alusten ja defujen lyhenteistä välilyönnit, muuten tulee ongelmia splittauksissa
  $välinpoistoEtsi = array("Ion C.", "Col. Ship", "Sol. Sat");
  $välinpoistoKorvaa = array("Ion_C.", "Col._Ship", "Sol._Sat");

  // alusten ja defujen lyhenteet ja niitä vastaavat nimet. oltava oikeassa järjestyksessäkin
  $lyhenteet["en"] = array(
	'R.Launcher' => 'Rocket Launcher',
	'L.Laser'    => 'Light Laser',
	'H.Laser'    => 'Heavy Laser',
	'Gauss'      => 'Gauss Cannon',
	'Ion_C.'     => 'Ion Cannon',//alunperin "Ion C."
	'Plasma'     => 'Plasma Turret',
	'S.Dome'     => 'Small Shield Dome',
	'L.Dome'     => 'Large Shield Dome',
	'S.Cargo'       => 'Small Cargo',
	'L.Cargo'       => 'Large Cargo',
	'L.Fighter'     => 'Light Fighter',
	'H.Fighter'     => 'Heavy Fighter',
	'Cruiser'       => 'Cruiser',
	'Battleship'    => 'Battleship',
	'Col._Ship'     => 'Colony Ship',//alunperin "Col. Ship"
	'Recy.'         => 'Recyler',
	'Esp.Probe'     => 'Espionage Probe',
	'Bomber'        => 'Bomber',
	'Sol._Sat'      => 'Solar Satellite',//alunperin "Sol. Sat"
	'Dest.'         => 'Destroyer',
	'Deathstar'     => 'Deathstar',
	'Battlecr.'     => 'Battlecruiser'
  );

  // vakoilurapsan tietoja ohjusiskua varten
  $vakoiluOtsikko = 'Resources at ([ \w\dåäö]+) (\[\d:\d+:\d+\]) \(Player \'([\w\dåäö]+)\'\) on (\d+-\d+ \d+:\d+:\d+)';// 0=kaikki, 1=planeetta, 2=naatit, 3=pelaaja, 4=aika
  $DEFUNIMET = array(
	'RH' => 'Rocket Launcher',
	'KL' => 'Light Laser',
	'RL' => 'Heavy Laser',
	'GT' => 'Gauss Cannon',
	'IT' => 'Ion Cannon',
	'PT' => 'Plasma Turret',
	'PS' => 'Small Shield Dome',
	'SS' => 'Large Shield Dome',
	'TO' => 'Anti-Ballistic Missiles',
	'PO' => 'Interplanetary Missiles'
  );
  $MISSILE_DEFUT = "Defense";
  $MISSILE_START = "Before attack at";
  $MISSILE_BETWEEN = "After %LKM% missiles...";
  $MISSILE_END = "Result at";
?>