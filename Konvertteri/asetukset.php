<?php
/**
 * Konvertterin asetukset
 * @author Kuitsi (http://iki.fi/kuitsi)
 * @license GPLv3 (see OTools/LICENSE.txt)
 */

include_once("colorsettings.php");

$erotin = "\n".str_repeat("-",50)."\n\n";

$lang = $r->getKieli();
if($lang=="en") {
  $hyökkääjä = "Attacker";
  $puolustaja = "Defender";
  $tuhottu = "Destroyed!";
  $kierroksiaTeksti = "After ".$r->getKierrokset()." rounds...";
  $kaappasi = "captured[\s\r\n]*";
  $menetti = "lost";
  $debrisformed = "At these";
  $kuu = "moon";
  $noDebris = " without DF";
  $withDebris = " / with DF: ";
  $recycledTotal = "recycled total of";
  $recycledMetal = "metal and";
  $recycledCrystal = "crystal";
  $syntyneestäRKsta = "of debris formed in this battle";
}
else/* if($lang=="fi") */ {
  $hyökkääjä = "Hyökkääjä";
  $puolustaja = "Puolustaja";
  $tuhottu = "Tuhottu!";
  $kierroksiaTeksti = $r->getKierrokset()." kierroksen jälkeen...";
  $kaappasi = "kaappasi[\s\r\n]*";
  $menetti = "menetti";
  $debrisformed = "Romukenttä|Näissä";// avaruuskoordinaateissa
  $kuu = "kuu";
  $noDebris = " ilman RK:ta";
  $withDebris = " / RK:lla: ";
  $recycledTotal = "kierrätti yhteensä";
  $recycledMetal = "metallia ja";
  $recycledCrystal = "kristallia";
  $syntyneestäRKsta = " tässä taistelussa syntyneestä RK:sta";
}
?>